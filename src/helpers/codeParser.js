export function fromConsoleToCode(consoleIn) {
  return consoleIn.split('\n')
    .filter((_, i, arr) => i !== 0 && i !== arr.length - 1)
    .join('\n');
}

export function createFunction(params, code) {
  return new Function([...params], code);
}

export function codeRunner(fn, cases) {
  return cases.map((testCase) => {
    const [args, expected] = testCase
      .split(' => ')
      .map((el, i) => !i? el.split(';').map(input => JSON.parse(input)): JSON.parse(el.trim()));

    return JSON.stringify(fn(...args)) === JSON.stringify(expected);
  });
}
