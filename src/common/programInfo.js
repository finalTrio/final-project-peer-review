const programInfo = {
  '1':{
    header: 'Beginner\'s Program',
    title: 'Welcome to our Beginner\'s Program!',
    body: 'Your first steps at becoming a JavaScript developer. We will guide you through fundamental concepts such as loops, arrays, functions, and more.',
    prices: 500,
    image: 'https://www.svgrepo.com/show/88453/web-development.svg',
  },
  '2':{
    header: 'Intermediate Program',
    title: 'Welcome to our Intermediate Program!',
    body: 'In this program we will go in depth into Functional and Object Oriented Programming, covering their characteristics, principles,  and best practices. We will also get familiar with more advanced data structures, have fun with matrices and much more!',
    prices: 1000,
    image: 'https://www.svgrepo.com/show/382092/graph-infographic-data-matrix-element.svg',
  },
  '3':{
    header: 'Advanced Program',
    title: 'Welcome to our Advanced Program!',
    body: 'Time to apply our knowledge in practice! During this program you will build your first web applications, using JavaScript, HTML, CSS, and one of the leading front-end frameworks in the industry- React.',
    prices: 1000,
    image: 'https://www.svgrepo.com/show/394388/react.svg',
  },
};

export default programInfo;
