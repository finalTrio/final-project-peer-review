export const userRole = {
  student: 'student',
  mentor: 'mentor',
  teacher: 'teacher',
  owner: 'owner'
};

