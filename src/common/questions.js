const questions = {
  '1': {
    question: 'Do I need previous experience with programming?',
    answer: 'Previous experience is helpful, but not necessary. You will learn everything you need to know to become a web developer during the program.'
  },
  '2': {
    question: 'What language is the program in?',
    answer: 'As most resources on programming are in English, as well as JavaScript itself, we will stick to it as the main language. Feel free to ask questions in Bulgarian, though.'
  },
  '3': {
    question: 'Does the program include one to one mentoring?',
    answer: 'Yes! Our mentors are here to help whenever you need them.'
  },

};

export default questions;
