import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';
// import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: 'AIzaSyDsRi9WNymOQ__8IsWBXg-U7CA07cLWe5o',
  authDomain: 'hogwarts-academy-22.firebaseapp.com',
  projectId: 'hogwarts-academy-22',
  storageBucket: 'gs://hogwarts-academy-22.appspot.com',
  messagingSenderId: '552942156260',
  appId: '1:552942156260:web:a7311a17c4f2239533aff1',
  measurementId: 'G-QLS5F9JH3H',
  databaseURL: 'https://hogwarts-academy-22-default-rtdb.europe-west1.firebasedatabase.app/'
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
// const analytics = getAnalytics(app);
