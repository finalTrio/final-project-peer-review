/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react';
import { Dropdown, DropdownButton } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../../../../context/AppContext';
import { updateMembers, getTeamById } from '../../../../services/teams.services';
import { getAllUsers } from '../../../../services/user.services';
import Loading from '../../../Loading/Loading';
import './AddMember.css';

const AddMember = () => {
  const { id } = useParams();
  const [team, setTeam] = useState([]);
  const [students, setStudents] = useState([]);
  const [members, setMembers] = useState([team.members]);
  const {userData} = useContext(AppContext);

  useEffect(() => {
    getTeamById(id)
      .then((team) => {
        setTeam(team);
        setMembers(team.members);
      });
  }, []);
  useEffect(()=>{
    getAllUsers()
      .then((s) => setStudents(s.filter(user=> user.role ==='student' && user.team === 'none')))
      .catch(console.error);
  }, []);

  const addMember = (e)=>{
    e.stopPropagation();
    const member= e.target.value;
    if (members.includes(member)) return;
    setMembers([...members, member]);
  };

  const removeMember=(e)=>{
    e.stopPropagation();

    const filtered = members.filter((t)=> t!==e.target.value);
    setMembers([...filtered]);
  };

  const update = () => {
    updateMembers(id, members).then(() => {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Members updated successfully!',
        showConfirmButton: false,
        timer: 1500
      });
    });
  };

  if (!userData) return <Loading title={' teams '}/>;
  if (!Object.keys(team).length) return <Loading title={' teams'} />;
  return (
    <div className='col py-3'>
      <div className='container team-details-box'>
        <h1 className='text-center mb-5 team-details-title'>{team.name}</h1>
        <div className='team-details-specs'>
          <h2>Team Specifications:</h2>
          <div>
            <h4 className='d-inline'>Members:</h4>
            <h5 className='d-inline'>&nbsp;{members.join(', ')}</h5>
          </div>
          <div>
            <h4 className='d-inline'>Teacher:</h4>
            <h5 className='d-inline'>&nbsp;{team.teacher}</h5>
          </div>
          <div>
            <h4 className='d-inline'>Workshops:</h4>
            {!team.workshops? <h5 className='d-inline'>&nbsp;No workshops yet.</h5> : <h5 className='d-inline'>{team.workshops.join(', ')}</h5>}
          </div>
        </div>
        {userData.role === 'admin' || userData.role === 'owner' || userData.role === 'teacher' &&
        <div className='row mb-3 team-details-update'>
          <h2>Update team</h2>
          <div className='col-sm'>
            <Dropdown className='addStudents'>
              <DropdownButton className = 'addStudentDrop' title="Add members" onChange={(e)=>e.stopPropagation()}>
                {students.map(student=> <Dropdown.Item className = 'addStudentItem'key={student.id}>
                  <div className='col'><span>{student.firstname} {student.lastname}
                    <button className= 'btn-primary student-add-btn' value={student.id}
                      onClick={members.includes(student.username) ? (e)=>removeMember(e):(e)=> addMember(e)}>{members.includes(student.username)? 'Remove' : 'Add'}
                    </button>
                  </span>
                  </div>
                </Dropdown.Item>)}
              </DropdownButton>
            </Dropdown>
          </div>
        </div>
        }
        <hr />
        {userData.role === 'admin' || userData.role === 'teacher' &&
        <button className='btn btn-outline-secondary update-btn' onClick={() => update()}>Update team</button>
        }
      </div>
    </div>
  );
};

export default AddMember;
