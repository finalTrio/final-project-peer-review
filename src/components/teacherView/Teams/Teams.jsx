import React, { useEffect, useState } from 'react';
import { getAllTeams } from '../../../services/teams.services';
import Loading from '../../Loading/Loading';
import './Teams.css';

import TeamCard from './TeamCard';

const Teams = () => {
  const [teams, setTeams] = useState([]);

  useEffect(()=>{
    getAllTeams()
      .then(setTeams)
      .catch(console.error);
  }, []);

  return (
    <div className='col py-3'>
      <h1 className='text-center'>Teams</h1>
      <p></p>
      <div className='container-fluid d-flex justify-content-around m-3'>
        <div className='row' >
          {!teams? <Loading/> : teams.length===0? <h4>No teams yet.</h4> :  teams.reverse().map(t=> <TeamCard key={t.id} team={t}/>)}
        </div>
      </div>
    </div>
  );
};

export default Teams;
