/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';
import './TeamCard.css';


const TeamCard = ({...props}) => {

  return (
    <div className='col-sm'>
      <img className="logo" src='https://www.svgrepo.com/show/324113/writing-education-learning-pencil-note-write-notebook.svg' style={{width:'380px', height: '222.5px'}} alt=""></img>
      <div className="blog-text">
        <h3>
          {props.team.name}
        </h3>
        <h6>Members: {props.team.members.join(', ')}</h6>
      </div>
      <Link to={`add-members/${props.team.id}`}><button className='btn btn-primary mb-3 view-team-btn'>View team</button></Link>
    </div>
  );
};
export default TeamCard;
