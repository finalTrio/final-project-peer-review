import React, { useContext, useEffect, useState } from 'react';
import Swal from 'sweetalert2';
// import {Multiselect} from 'multiselect-react-dropdown';
import { createTeam, getAllTeams } from '../../../services/teams.services';
import { getAllUsers } from '../../../services/user.services';
import Loading from '../../Loading/Loading';
import AppContext from '../../../context/AppContext';
// import { NavDropdown} from 'react-bootstrap';
import { Dropdown } from 'react-bootstrap';
import { DropdownButton } from 'react-bootstrap';
import './CreateTeam.css';
import { useNavigate } from 'react-router';

const Teams = () => {
  // const [name, setName] = useState();
  const [students, setStudents] = useState([]);
  // const [members, setMembers] = useState();
  const {user, userData} = useContext(AppContext);
  const [team, setTeam] = useState({
    name: '',
    teacher: user,
    members:[],
  });
  const [teams, setTeams] = useState([]);
  const navigate = useNavigate();

  useEffect(()=>{
    getAllTeams()
      .then(setTeams)
      .catch(console.error);
    getAllUsers()
      .then((res)=> {
        setStudents(res.filter(user=> user.role === 'student'));
      })
      .then(getAllTeams)
      .catch(console.error);
  }, []);

  const addMember = (e)=>{
    e.stopPropagation();
    const member= e.target.value;
    setTeam({...team, members:[...team.members, member]});
    // e.target.value='';
  };


  const removeMember=(e)=>{
    e.stopPropagation();
    const filtered = team.members.filter((t)=> t!==e.target.value);
    setTeam({...team, members:[...filtered]});

  };


  // getAllTeams().then((snapshot) => setTeams(snapshot.val()));

  const create = (team) => {
    if (!team.name)  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please add name!',
      });
      return;
    }

    if (teams.find(t=> t.name ===team.name))  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'A team with this name already exists!',
      });
      return;
    }
    setTeam({...team, teacher:userData.username});

    return createTeam(team)
      .then(() => {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Team created successfully!',
          showConfirmButton: false,
          timer: 1500
        });
        navigate('/teams');
      })
      .catch(console.error);
  };

  return (
    <div className='col py-3'>
      <div className='container create-task-box'>
        <h1 className='text-center create-team-title'>Create a new team</h1>
        <p></p>
        <div className='container-fluid create-team-box'>
          <div className='add-team-title'>
            <h3>Add Team Name</h3>
            <textarea rows="1" className="form-control team-title-form" name="description" value={team.name} onChange={(e) => setTeam({...team, name:e.target.value})} ></textarea>
          </div>
          <div className='add-students-section'>
            <h3>Add Students</h3>
            <div className='container-fluid student-list' style={{position: 'relative', display: 'block'}}>
              {!students? <Loading/>: <Dropdown className='student-dropdown'>
                <DropdownButton className = 'student-dropdown-button' title="Add students" onChange={(e)=>e.stopPropagation()}>
                  {students.map(student=> <Dropdown.Item className = 'student-dropdown-item'key={student.id}>
                    <div className='col'><span>{student.firstname} {student.lastname}
                      <button className= 'btn-primary student-add-btn' value={student.id}
                        onClick={team.members.includes(student.id) ? (e)=>removeMember(e):(e)=> addMember(e)}>{team.members.includes(student.id)? 'Remove' : 'Add'}
                      </button>
                    </span>
                    </div>
                  </Dropdown.Item>)}
                </DropdownButton>
              </Dropdown>}
            </div>
          </div>
          <hr />
          {!user? <Loading/> : <button type="submit" className='btn btn-outline-primary create-btn' data-bs-dismiss="modal" onClick={()=> create(team)}>Create team</button>}
        </div>
      </div>
    </div>
  );
};

export default Teams;

