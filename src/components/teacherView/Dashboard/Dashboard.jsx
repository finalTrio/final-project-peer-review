import React, { } from 'react';
// import {
//   Chart as ChartJS,
//   CategoryScale,
//   LinearScale,
//   BarElement,
//   Title,
//   Tooltip,
//   Legend,
// } from 'chart.js';
// import { Bar } from 'react-chartjs-2';

import './Dashboard.css';
import TeacherCard from '../TeacherCard/TeacherCard';

const Dashboard = () => {

  // ChartJS.register(
  //   CategoryScale,
  //   LinearScale,
  //   BarElement,
  //   Title,
  //   Tooltip,
  //   Legend
  // );

  // const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  // const options = {
  //   responsive: true,
  //   plugins: {
  //     legend: {
  //       position: 'top',
  //     },
  //     title: {
  //       display: true,
  //       text: 'Chart.js Bar Chart',
  //     },
  //   },
  // };


  // const data = {
  //   labels,
  //   datasets: [
  //     {
  //       label: 'Dataset 1',
  //       data: labels.map(() => Math.ceil(Math.random() * 1000)),
  //       backgroundColor: 'rgba(255, 99, 132, 0.5)',
  //     },
  //     {
  //       label: 'Dataset 2',
  //       data: labels.map(() => Math.ceil(Math.random() * 1000)),
  //       backgroundColor: 'rgba(53, 162, 235, 0.5)',
  //     },
  //   ],
  // };

  return (
    <div className='col py-3'>
      <h1 className='text-center'>Admin panel</h1>
      <TeacherCard />
    </div>
  );
};

export default Dashboard;
