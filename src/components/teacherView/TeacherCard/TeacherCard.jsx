import React, { useEffect, useState } from 'react';
import { getAllComments } from '../../../services/comment.services';
import { getAllPosts } from '../../../services/posts.services';
import { getAllTasks } from '../../../services/task.services';
import { getAllUsers } from '../../../services/user.services';
import { getAllWorkshops } from '../../../services/workshop.services';
import Loading from '../../Loading/Loading';
import './TeacherCard.css';
const TeacherCard = () => {
  const [posts, setPosts] = useState();
  const [comments, setComments] = useState();
  const [workshops, setWorkshops] = useState();
  const [tasks, setTasks] = useState();
  const [users, setUsers] = useState();
  useEffect(() => {
    getAllPosts()
      .then(setPosts);
  }, []);
  useEffect(() => {
    getAllComments()
      .then(setComments);
  }, []);
  useEffect(() => {
    getAllWorkshops()
      .then(setWorkshops);
  }, []);
  useEffect(() => {
    getAllTasks()
      .then(setTasks);
  }, []);
  useEffect(() => {
    getAllUsers()
      .then(setUsers);
  }, []);

  if (!posts) return <Loading title={'statistics'} />;
  if (!comments) return <Loading title={'statistics'} />;
  if (!workshops) return <Loading title={'statistics'} />;
  if (!tasks) return <Loading title={'statistics'} />;
  if (!users) return <Loading title={'statistics'} />;

  return (
    <div>
      <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/bootstrap-extended.min.css" />
      <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/fonts/simple-line-icons/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/colors.min.css" />
      <div className="container-fluid">
        <section id="minimal-statistics">
          <div className="row">
            <div className="col-12 mt-3 mb-1">
              <h4 className="text-uppercase">User and Forum</h4>
              <p>Statistics</p>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="media-body text-left">
                        <h3 className="primary">{posts.length + 15}</h3>
                        <span>New Posts</span>
                      </div>
                      <div className="align-self-center">
                        <i className="icon-book-open primary font-large-2 float-right ms-4"></i>
                      </div>
                    </div>
                    <div className="progress mt-1 mb-0" style={{ height: '7px' }}>
                      <div className="progress-bar bg-primary" role="progressbar" style={{ width: '80%' }} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="media-body text-left">
                        <h3 className="warning">{comments.length + 7}</h3>
                        <span>New Comments</span>
                      </div>
                      <div className="align-self-center">
                        <i className="icon-bubbles warning font-large-2 float-right ms-4"></i>
                      </div>
                    </div>
                    <div className="progress mt-1 mb-0" style={{ height: '7px' }}>
                      <div className="progress-bar bg-warning" role="progressbar" style={{ width: '35%' }} aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="align-self-center">
                        <i className="icon-graph success font-large-2 float-left me-4"></i>
                      </div>
                      <div className="media-body text-right">
                        <h3>{tasks.length + 12}</h3>
                        <span>New tasks</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="align-self-center">
                        <i className="icon-pointer danger font-large-2 float-left me-4"></i>
                      </div>
                      <div className="media-body text-right">
                        <h3>423</h3>
                        <span>Total Visits</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="media-body text-left">
                        <h3 className="danger">{workshops.length + 12}</h3>
                        <span>New Projects</span>
                      </div>
                      <div className="align-self-center">
                        <i className="icon-graph danger font-large-2 float-right ms-4"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="media-body text-left">
                        <h3 className="success">{users.length + 19}</h3>
                        <span>New Students</span>
                      </div>
                      <div className="align-self-center">
                        <i className="icon-user success font-large-2 float-right ms-4"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="media-body text-left">
                        <h3 className="warning">96.32 %</h3>
                        <span>Success rate </span>
                      </div>
                      <div className="align-self-center">
                        <i className="icon-pie-chart warning font-large-2 float-right ms-4"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-sm-6 col-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body">
                    <div className="media d-flex">
                      <div className="media-body text-left">
                        <h3 className="danger">13</h3>
                        <span>Total Awards</span>
                      </div>
                      <div className="align-self-center">
                        <i className="icon-rocket danger font-large-2 float-right ms-4"></i>
                      </div>
                    </div>
                    <div className="progress mt-1 mb-0" style={{ height: '7px' }}>
                      <div className="progress-bar bg-danger" role="progressbar" style={{ width: '60%' }} aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="stats-subtitle">
          <div className="row">
            <div className="col-12 mt-3 mb-1">
              <h4 className="text-uppercase">Statistics </h4>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-6 col-md-12">
              <div className="card overflow-hidden">
                <div className="card-content">
                  <div className="card-body cleartfix">
                    <div className="media align-items-stretch">
                      <div className="align-self-center">
                        <i className="icon-pencil primary font-large-2 mr-2"></i>
                      </div>
                      <div className="media-body">
                        <h4>Total Posts</h4>
                        <span>Monthly blog posts</span>
                      </div>
                      <div className="align-self-center">
                        <h1>{posts.length + 90}</h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-6 col-md-12">
              <div className="card">
                <div className="card-content">
                  <div className="card-body cleartfix">
                    <div className="media align-items-stretch">
                      <div className="align-self-center">
                        <i className="icon-speech warning font-large-2 mr-2"></i>
                      </div>
                      <div className="media-body">
                        <h4>Total Comments</h4>
                        <span>Monthly blog comments</span>
                      </div>
                      <div className="align-self-center">
                        <h1>{comments.length + 54}</h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default TeacherCard;
