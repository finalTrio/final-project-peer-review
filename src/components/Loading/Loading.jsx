/* eslint-disable react/prop-types */
//Libraries
import React from 'react';
//CSS
import './Loading.css';
const Loading = ({title}) => {
  return (
    <><h1 className='text-center'>Loading {title}.</h1>
      <div className='d-flex justify-content-center' style={{ marginBottom: '130px', marginTop: '50px' }}>
        <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
      </div></>
  );
};

export default Loading;
