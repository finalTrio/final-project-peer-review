import React from 'react';
import DashExam from '../Course/Exams/DashExam';
import DashTasks from '../Course/Tasks/DashTasks/DashTasks';
import './DashBoard.css';
const DashBoard = () => {
  return (
    <div className="col py-3">
      <div className='dash' style={{marginTop: '45px'}}>
        <h1 className='text-center'>Course overview</h1>
        <DashTasks />
        <DashExam />
      </div>
    </div>
  );
};

export default DashBoard;
