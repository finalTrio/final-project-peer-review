/* eslint-disable react/prop-types */
import React from 'react';

const TestCase = ({index, description, casePoints, points, setPoints}) => {

  const updatePoints = (val) => {
    const newPoints = [...points];
    newPoints[index] = val? casePoints: 0;
    setPoints([...newPoints]);
  };

  return (
    <div className='row mt-2 mb-1 d-flex justify-content-center'>
      <div className='col-8 me-2 info'>
        <h4 className='text-start' >Description: <span style={{fontSize: '17px'}}>{description}</span></h4>
      </div>
      <div className='col-3 me-2 text-center points'>
        <h4> {<span><button className={`btn ${points[index]? 'btn-success':'btn-light'} mt-1`} onClick={() => updatePoints(true)}>Yes</button> <button className={`btn ${!points[index] && points[index] !== undefined? 'btn-danger':'btn-light'} mt-1`} onClick={() => updatePoints(false)}>No</button></span>}</h4>
        <h6 style={{fontSize: '10px', marginTop: '-8px'}}>You selected: {points[index]? 'Yes': points[index] === undefined? '-': 'No'}</h6>
      </div>
    </div>
  );
};

export default TestCase;
