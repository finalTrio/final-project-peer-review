import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import AppContext from '../../../../../context/AppContext';
import { getAllWorkshops } from '../../../../../services/workshop.services';
import SingleExam from '../SingleExam/SingleExam';
import './AllExams.css';

const AllExams = () => {
  const { userData } = useContext(AppContext);
  const [exams, setExams] = useState([]);

  useEffect(() => {
    getAllWorkshops()
      .then((workshops) => setExams([...workshops].filter((exam) => exam.teams.includes(userData.team))));
  }, [userData]);



  return (
    <div className='col py-3'>
      <div className='container-fluid m-3'>
        <h1>Exams</h1>
        <div className='row exam'>
          {exams.length === 0 ?
            <p>No exams to show.</p> :
            exams
              .map((exam) => <SingleExam key={exam.id} exam={exam} teamId={userData.team} />)
          }
        </div>
      </div>
    </div>

  );
};

export default AllExams;
