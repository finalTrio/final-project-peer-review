/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';

const SingleExam = ({exam, teamId}) => {
  const date = exam.createdOn;
  const month = date.getMonth() + 2;
  let getMonth = '';
  const day = date.getDate();
  switch (month) {
  case 1 : getMonth = 'January'; break;
  case 2 : getMonth = 'February'; break;
  case 3 : getMonth = 'March'; break;
  case 4 : getMonth = 'April'; break;
  case 5 : getMonth = 'May'; break;
  case 6 : getMonth = 'June'; break;
  case 7 : getMonth = 'July'; break;
  case 8 : getMonth = 'August'; break;
  case 9 : getMonth = 'September'; break;
  case 10 : getMonth = 'October'; break;
  case 11 : getMonth = 'November'; break;
  case 12 : getMonth = 'December'; break;
  default : getMonth = 'Never';
  }

  return (
    <div className="col-lg-4 mt-3">
      <div className="card card-margin">
        <div className="card-header no-border">
          <h5 className="card-title">{exam.name}</h5>
        </div>
        <div className="card-body pt-0">
          <div className="widget-49">
            <div className="widget-49-title-wrapper">
              <div className="widget-49-date-primary">
                <span className="widget-49-date-day">{day}</span>
                <span className="widget-49-date-month">{getMonth}</span>
              </div>
              <div className="widget-49-meeting-action">
                <Link to={`../exam/${exam.name.replace(' ', '-')}/${exam.id}/${teamId}`} className="btn btn-sm btn-flash-border-primary">See workshop</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SingleExam;
