import React from 'react';
import { Link } from 'react-router-dom';
import ExamCard from './ExamCard/ExamCard';

const Course = () => {
  return (
    <div className='container-fluid m-3'>
      <h2>Workshops</h2>
      <div className='row'>
        <ExamCard />
        <ExamCard />
        <ExamCard />
        <Link to={'/exams'}><p>See all...</p></Link>
      </div>
    </div>
  );
};

export default Course;
