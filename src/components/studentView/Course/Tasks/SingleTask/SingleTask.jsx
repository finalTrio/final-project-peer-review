/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';

import './SingleTask.css';

const SingleTask = ({task}) => {
  const date = new Date(task.createdOn);
  const month = date.getMonth() + 2;
  let getMonth = '';
  const day = date.getDate();

  switch (month) {
  case 1 : getMonth = 'January'; break;
  case 2 : getMonth = 'February'; break;
  case 3 : getMonth = 'March'; break;
  case 4 : getMonth = 'April'; break;
  case 5 : getMonth = 'May'; break;
  case 6 : getMonth = 'June'; break;
  case 7 : getMonth = 'July'; break;
  case 8 : getMonth = 'August'; break;
  case 9 : getMonth = 'September'; break;
  case 10 : getMonth = 'October'; break;
  case 11 : getMonth = 'November'; break;
  case 12 : getMonth = 'December'; break;
  default : getMonth = 'Never';
  }

  return (
    <div className="col-lg-4 mt-3">
      <div className="card card-margin">
        <div className="card-header no-border">
          <h5 className="card-title">{task.title}</h5>
        </div>
        <div className="card-body pt-0">
          <div className="widget-49">
            <div className="widget-49-title-wrapper">
              <div className="widget-49-date-primary">
                <span className="widget-49-date-day">{day}</span>
                <span className="widget-49-date-month">{getMonth}</span>
              </div>
              <div className="widget-49-meeting-info">
                <span className="widget-49-pro-title">{!task.tags ? '' : task.tags.join(', ')}</span>
                <span className="widget-49-meeting-time">Level: {task.level}</span>
              </div>
            </div>
            <ul className="widget-49-meeting-points">
              <li className="widget-49-meeting-item"><span>Description: {task.description.slice(0, 20)}...</span></li>
              <li className="widget-49-meeting-item"><span>Constraints: {task.constraints.slice(0, 20)}...</span></li>
              <li className="widget-49-meeting-item"><span>Example: {task.example}</span></li>
            </ul>
            <div className="widget-49-meeting-action">
              <Link to={`../task/${task.id}`} className="btn btn-sm btn-flash-border-primary">Complete task</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SingleTask;
