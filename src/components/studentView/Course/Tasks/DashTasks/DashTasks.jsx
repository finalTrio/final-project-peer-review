import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getAllTasks } from '../../../../../services/task.services';
import SingleTask from '../SingleTask/SingleTask';
//TODO Slice the last 3 tasks
const DashTasks = () => {
  const [tasks, setTasks] = useState([]);
  useEffect(() => {
    getAllTasks()
      .then(setTasks)
      .catch(console.error);
  }, []);

  return (
    <div className='container-fluid m-3'>
      <h2>Tasks</h2>
      <div className='row'>
        {tasks.map((m) => <SingleTask key={m.key} task={m}/>)}
        <Link to={'/tasks'}><p>See all...</p></Link>
      </div>
    </div>
  );
};

export default DashTasks;
