import React, { useEffect, useState } from 'react';
import { getAllTasks } from '../../../../../services/task.services';
import SingleTask from '../SingleTask/SingleTask';
import './AllTasks.css';
const AllTasks = () => {
  const [tasks, setTasks] = useState([]);
  const [filters, setFilters] = useState(['Beginner','Intermediate','Advanced']);
  useEffect(() => {
    getAllTasks()
      .then(setTasks)
      .catch(console.error);
  }, []);

  return (
    <div className='col py-3'>
      <div className='container-fluid m-3'>
        <h1>Tasks</h1>
        <span>
          <button className='task-sorter' onClick={()=> setFilters(['Beginner','Intermediate','Advanced'])}>All</button> |&nbsp;
          <button className='task-sorter' onClick={()=> setFilters(['Beginner'])}>Beginner</button> |&nbsp;
          <button className='task-sorter' onClick={()=> setFilters(['Intermediate'])}>Intermediate</button> |&nbsp;
          <button className='task-sorter' onClick={()=> setFilters(['Advanced'])}>Advanced</button>
        </span>
        <div className='row tasks'>
          {tasks.length === 0 ?
            <p>No posts to show.</p> :
            tasks.filter((task)=> filters.includes(task.level))
              .map((task)=><SingleTask key ={task.id} task={task}/>)
          }
        </div>
      </div>
    </div>

  );
};

export default AllTasks;
