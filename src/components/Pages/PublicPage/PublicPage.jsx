/* eslint-disable react/prop-types */
import React from 'react';
import HomeHeader from '../../home/HomeHeader/HomeHeader';
import HomeFooter from '../../home/HomeFooter/HomeFooter';

const PublicPage = ({children}) => {
  return (
    <>
      <HomeHeader />
      {children}
      <HomeFooter />
    </>
  );
};

export default PublicPage;
