/* eslint-disable react/prop-types */
import React from 'react';
import Header from '../../privateHome/privateHeader/Header';
import Sidebar from '../../privateHome/sideBar/SideBar';

const TeacherPage = ({children, visibility=false}) => {
  return (
    <>
      <Header />
      <div className="container-fluid" >
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"/>
        <div className="row flex-nowrap">
          <Sidebar visibility={visibility}/>
          {children}
        </div>
      </div>
    </>
  );
};

export default TeacherPage;
