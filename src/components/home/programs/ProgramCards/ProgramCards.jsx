import React from 'react';

import './ProgramCards.css';
import programInfo from '../../../../common/programInfo';
import Modal from '../../../../views/Modal/Modal';

const ProgramCards = () => {

  return (
    <section id='programs'>
      <h2 className='text-center text-title'>Programs</h2>
      <div className="container me-4 ms-4 mt-5 example-card">
        <section className="light">
          <article className="postcard light blue">
            <a className="postcard__img_link">
              <img className="postcard__img" src="https://img.freepik.com/free-photo/concept-technology-internet-networking-hand-touching-digital-media-icon-with-show-display-tablet_2034-2453.jpg?w=2000" alt="Image Title" />
            </a>
            <div className="postcard__text t-dark">
              <h1 className="postcard__title blue"><a>{programInfo[1].header}</a></h1>
              <div className="postcard__subtitle small">
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">{programInfo[1].body}</div>
              <ul className="postcard__tagbox">
                <button id='1'  type="button" className="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#myModal1">Learn More</button>
              </ul>
            </div>
          </article>
          <article className="postcard light blue">
            <a className="postcard__img_link">
              <img className="postcard__img" src="https://www.udacity.com/blog/wp-content/uploads/2021/01/Become-a-full-stack-web-developer_Blog-scaled.jpeg" alt="Image Title" />
            </a>
            <div className="postcard__text t-dark">
              <h1 className="postcard__title red"><a>{programInfo[2].header}</a></h1>
              <div className="postcard__subtitle small">
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">{programInfo[2].body}</div>
              <ul className="postcard__tagbox">
                {/* <button id='2' onClick ={(e)=>openWindow(e.target.id)}className="btn btn-secondary active">Learn more</button> */}
                <button id='2'  type="button" className="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#myModal2">Learn More</button>
              </ul>
            </div>
          </article>
          <article className="postcard light blue">
            <a className="postcard__img_link">
              <img className="postcard__img" /*src="https://picsum.photos/500/501"*/ src='https://www.thetrainingroom.com/getattachment/Course-sectors/IT/Web-Dev-E-Learning/web-development-course.jpgaspx' alt="Image Title" />
            </a>
            <div className="postcard__text t-dark">
              <h1 className="postcard__title green"><a>{programInfo[3].header}</a></h1>
              <div className="postcard__subtitle small">
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">{programInfo[3].body}</div>
              <ul className="postcard__tagbox">
                <button id='3'  type="button" className="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#myModal3">Learn More</button>
              </ul>
            </div>
          </article>
        </section>
      </div>
      <Modal />
    </section>
  );
};

export default ProgramCards;
