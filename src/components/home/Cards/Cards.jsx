import React from 'react';
import './Cards.css';
import HomeCards from '../HomeCards/HomeCards';
import OurFacts from '../OurFacts/OurFacts';
import ExampleCards from '../programs/ProgramCards/ProgramCards';
import Carousel from '../Carousel/Carousel';
import AboutUs from '../AboutUs/AboutUs';
import FrequentQuestions from '../FAQ/FrequentQuestions';
const Cards = () => {
  return (
    <div className= 'top'>
      <div className="container cards" >
        <div className="row">
          <HomeCards />
        </div>
      </div>
      <Carousel />
      <div className="container cards" >
        <ExampleCards />
      </div>
      <OurFacts/>
      <FrequentQuestions />
      <AboutUs />
    </div>
  );
};

export default Cards;
