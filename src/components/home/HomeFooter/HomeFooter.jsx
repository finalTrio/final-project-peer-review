import React from 'react';
import './HomeFooter.css';

const HomeFooter = () => {
  return (
    <footer className="footer-06">
      <div className="container-fluid">
        <div className="row pt-4">
          <div className="col-md-3 col-lg-6 order-md-last">
            <div className="row justify-content-end">
              <div className="col-md-12 col-lg-9 text-md-right mb-md-0 mb-4">
                <h2 className="footer-heading"><a href="#" className="logo">Colorlib</a></h2>
                <p className="copyright">
                Copyright &copy;{new Date().getFullYear()} All rights reserved | This template is made with <i className="ion-ios-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" rel="noreferrer">Colorlib.com</a>
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-9 col-lg-6">
            <div className="row">
              <div className="col-md-4 mb-md-0 mb-4">
                <h2 className="footer-heading">Information</h2>
                <ul className="list-unstyled">
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Our Company</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Data</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Pricing</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Contact Us</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Support</a></li>
                </ul>
              </div>
              <div className="col-md-4 mb-md-0 mb-4">
                <h2 className="footer-heading">Application</h2>
                <ul className="list-unstyled">
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Download</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Bike Provider</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>How to Used</a></li>
                </ul>
              </div>
              <div className="col-md-4 mb-md-0 mb-4">
                <h2 className="footer-heading">API</h2>
                <ul className="list-unstyled">
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Documentation</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Credential</a></li>
                  <li><a href="#" className="py-1 d-block"><span className="ion-ios-checkmark-circle-outline mr-2"></span>Developer info</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

  );
};

export default HomeFooter;
