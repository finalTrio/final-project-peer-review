import React from 'react';
import '../AboutUs/AboutUs.css';
import alex from'../../../images/alex.jpg';
import nikol from'../../../images/nikol.jpg';
import Vi from'../../../images/Vi.jpg';
const AboutUs = () => {
  return (
    <div className="container" id='aboutUs'>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
      <div className="row justify-content-center">
        <div className="col-12 col-sm-8 col-lg-6 mt-5">
          <div className="section_heading text-center wow fadeInUp mt-5" data-wow-delay="0.2s">
            <h1 className='aboutUsTitle mb-5'>Our Developers<span> Team</span></h1>
            <div className="line"></div>
          </div>
        </div>
      </div>
      <div className="row justify-content-center">
        {/* TODO Vi- Must add the correct nane, photo, facebook and cv link*/}
        <div className="col-12 col-sm-6 col-lg-3">
          <div className="single_advisor_profile wow fadeInUp" data-wow-delay="0.2s">

            <div className="advisor_thumb"><img src={Vi} style={{width: '115.75%'}} alt=""/>
              <div className="social-info"><a href="#"><i className="fa fa-facebook"></i></a><a href="#"><i className="fa fa-twitter"></i></a><a href="#"><i className="fa fa-linkedin"></i></a></div>
            </div>
            <div className="single_advisor_details_info">
              <h6>Vitalia Gerenska</h6>
              <p className="designation">Founder &amp; CEO</p>
            </div>
          </div>
        </div>
        {/* TODO Alex- Must add the correct nane, photo, facebook and cv link*/}
        <div className="col-12 col-sm-6 col-lg-3">
          <div className="single_advisor_profile wow fadeInUp" data-wow-delay="0.3s">

            <div className="advisor_thumb"><img src={alex} style={{width: '142.5%'}} alt=""/>

              <div className="social-info"><a href="https://instagram.com/a_hj_photography?igshid=YmMyMTA2M2Y="><i className="fa fa-instagram"></i></a><a href="#"><i className="fa fa-twitter"></i></a><a href="#"><i className="fa fa-linkedin"></i></a></div>
            </div>

            <div className="single_advisor_details_info">
              <h6>Alex</h6>
              <p className="designation">Founder &amp; CEO</p>
            </div>
          </div>
        </div>
        {/* TODO Nikol- Must add the correct nane, photo, facebook and cv link*/}
        <div className="col-12 col-sm-6 col-lg-3">
          <div className="single_advisor_profile wow fadeInUp" data-wow-delay="0.4s">

            <div className="advisor_thumb"><img src={nikol} style={{width: '120%'}} alt=""/>

              <div className="social-info"><a href="https://www.facebook.com/nikolnikolova00"><i className="fa fa-facebook"></i></a><a href="#"><i className="fa fa-twitter"></i></a><a href="#"><i className="fa fa-linkedin"></i></a></div>
            </div>

            <div className="single_advisor_details_info">
              <h6>Nikol Nikolova</h6>
              <p className="designation">Founder &amp; CEO</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
