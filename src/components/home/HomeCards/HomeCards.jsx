import React, { useEffect, useState } from 'react';

const HomeCards = () => {
  let [countTeachers, setCountTeachers] = useState(0);
  let [countPercent, setCountPercent] = useState(0);
  let [countStudents, setCountStudents] = useState(0);
  let [countAwards, setCountAwards] = useState(0);
  useEffect(() => {
    setTimeout(() => {
      setCountTeachers((countTeachers) => countTeachers < 8 ? countTeachers + 1 : countTeachers);
    }, 20);
  }, [countTeachers]);

  useEffect(() => {
    setTimeout(() => {
      setCountPercent((countPercent) => countPercent < 97 ? countPercent + 1 : countPercent);
    }, 20);
  }, [countPercent]);

  useEffect(() => {
    setTimeout(() => {
      setCountStudents((countStudents) => countStudents < 27 ? countStudents + 1 : countStudents);
    }, 20);
  }, [countStudents]);

  useEffect(() => {
    setTimeout(() => {
      setCountAwards((countAwards) => countAwards < 13 ? countAwards + 1 : countAwards);
    }, 15);
  }, [countAwards]);

  return (
    <div className="container" style= {{marginTop: '-85px', position: 'relative'}} >
      <div className="row">
        <div className="col">
          <div className="count-area-content">
            <div className="count-digit">{countTeachers}</div>
            <div className="count-title">Current Teachers</div>
          </div>
        </div>
        <div className="col">
          <div className="count-area-content percentage">
            <div className="count-digit">{countPercent}</div>
            <div className="count-title">Succeeded Students</div>
          </div>
        </div>
        <div className="col">
          <div className="count-area-content">
            <div className="count-digit">{countStudents}</div>
            <div className="count-title">New Students</div>
          </div>
        </div>
        <div className="col">
          <div className="count-area-content">
            <div className="count-digit">{countAwards}</div>
            <div className="count-title">Awards</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeCards;
