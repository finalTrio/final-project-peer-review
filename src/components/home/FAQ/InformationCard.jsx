import React from 'react';
import questions from '../../../common/questions';
import PropTypes from 'prop-types';
import './InformationCard.css';

const InformationCard = ({cardId}) => {
  return (
    <section className="py-2" style={{width: '80%', margin: 'auto'}}>
      <div className="container">
        <div className="col mx-auto">
          <blockquote className="blockquote blockquote-custom bg-white p-5 shadow rounded">
            <header><h3>{questions[cardId].question}</h3></header>
            <div className="blockquote-custom-icon bg-info shadow-sm"><i className="fa fa-quote-left text-white"></i></div>
            <p className="mb-0 mt-2 font-italic">{questions[cardId].answer}</p>
          </blockquote>
        </div>
      </div>
    </section>

  );
};

InformationCard.propTypes = {
  cardId: PropTypes.string,
};

export default InformationCard;

// <div className='container-fluid info-card'>
//   <div className='row'>
//     <h3 style={{marginTop: '2%'}}>{questions[cardId].question}</h3>
//   </div>
//   <hr />
//   <div className='row'>
//     <p>{questions[cardId].answer}</p>
//   </div>
// </div>
