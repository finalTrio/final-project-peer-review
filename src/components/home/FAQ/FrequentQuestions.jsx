import React, { useState } from 'react';
// import QuestionCard from './QuestionCard';
import questions from '../../../common/questions';
import InformationCard from './InformationCard';
import './FrequentQuestions.css';

const FrequentQuestions = () => {

  const [answerId, setAnswerId] = useState(null);
  //TODO With useState, open it on scroll
  return (
    <div className='container q-outer-box mt-5' id={'FAQ'}>
      <div className='innerFAQ'>
        <div className='row mb question-box' data-toggle="collapse" data-target="#collapseContent" aria-expanded="true" aria-controls="collapseContent">
          <h1 className='titleFAQ'>FAQ</h1>
        </div>
        <div className='container-xxl question-box-content'>
          <div className='row mt-5' id='collapseContent' style={{transition: 'all 0.4s ease-in-out'}}>
            <h1 className='questions-header'>You asked. We answered.</h1>
            <div className='col col-lg-4 question-section'>
              <div id="accordion" style={{width: '90%', margin: 'auto auto'}}>
                <button className='question-button btn' id = '1' onClick={(e)=>setAnswerId(e.target.id)} data-toggle="button">{questions[1].question}</button>
                <button className='question-button btn' id = '2' onClick={(e)=>setAnswerId(e.target.id)} data-toggle="button">{questions[2].question}</button>
                <button className='question-button btn' id = '3' onClick={(e)=>setAnswerId(e.target.id)} data-toggle="button">{questions[3].question}</button>
                {/* <button className='question-button btn mb-4' id = '3' onClick={(e)=>setAnswerId(e.target.id)} data-toggle="button">{questions[3].question}</button> */}
              </div>
            </div>
            <div className='col'>
              <InformationCard cardId ={answerId? answerId: '1'}/>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
};

export default FrequentQuestions;
