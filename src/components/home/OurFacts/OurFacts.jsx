//Libraries
import React from 'react';
//CSS
import './OurFacts.css';
const OurFacts = () => {
  return (
    <div className="container-fluid our-facts">
      <div className='container-fluid inner mt-4' >
        <div className="col-lg-12 mb-4 mt-4">
          <h2 className='text-center ourTitle' >A Few Facts About Our Academy</h2>
        </div>
        <div className="row out">
          <div>
            <div className="row mb-4 ">
              <div className="col-md">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-text text-center" >We work with some of the best JavaScript technical trainers in Bulgaria!
                    </h4>

                  </div>
                  <img className="rounded ourPhoto mx-auto d-block mb-3" src=
                    "https://www.heraldscotland.com/resources/images/7048291/"
                  alt="Card image" />
                </div>
              </div>
              <div className="col-md">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-text text-center">Most of the projects throughout the program are teamwork-based.
                    </h4>
                  </div>
                  <img className="rounded ourPhoto mx-auto d-block mb-3" src=
                    "https://resume-example.com/wp-content/uploads/2021/03/teamwork.jpg"
                  alt="Card image" />
                </div>
              </div>
              <div className="col-md">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-text text-center">We provide an internationally-recognized certificate of completion.
                    </h4>

                  </div>
                  <img className="rounded ourPhoto3 mx-auto d-block mb-3" src=
                    "https://www.billingsfcu.org/wp-content/uploads/GraduationHatsThrownIntoTheAir-1400x891.jpg"
                  alt="Card image"/>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default OurFacts;
