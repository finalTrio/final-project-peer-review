/* eslint-disable react/prop-types */
//Libraries
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { HashLink as Link } from 'react-router-hash-link';
//Components

//CSS
import './HomeHeader.css';
import logo from '../../../images/logo.png';
import logoOrange from '../../../images/logo2.png';
const HomeHeader = () => {
  const navigate = useNavigate();
  const [showButton, setShowButton] = useState(true);

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.pageYOffset < 740) {
        setShowButton(true);
      } else if (window.pageYOffset > 740) {
        setShowButton(false);
      }
    });
  }, []);
  //TODO Must find a better name for about us, like 'Team ' 'Meet us' and etc.
  return (
    <>
      <Navbar className={showButton ? 'sticky-top text-center custom' : 'sticky-top text-center custom2'} collapseOnSelect expand="lg">
        <Container>
          <Navbar.Brand onClick={() => navigate('/home')}>{showButton ? <img src={logoOrange} className='logo'/> : <img src={logo} className='logo'/>}</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Link to='#about' smooth >{<h3 className='navInfo' >About</h3>}</Link>
              <Link to='#programs' smooth >{<h3 className='navInfo' >Programs</h3>}</Link>
              <Link to='#FAQ' smooth >{<h3 className='navInfo' >FAQ</h3>}</Link>
              <Link to='#aboutUs' smooth >{<h3 className='navAboutUs'>About us</h3>}</Link>
            </Nav>
            <Nav>
              <NavDropdown title="Profile" className='navProfile' id="collasible-nav-dropdown">
                <NavDropdown.Item  onClick={() => navigate('/login')}>Log in</NavDropdown.Item>
                <NavDropdown.Item  onClick={() => navigate('/register')}>Register</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
export default HomeHeader;
