import React from 'react';
import '../Carousel/Carousel.css';
const Carousel = () => {
  return (
    <section className="pt-5 pb-2 mt-5 back" id='about'>
      <div className="container">
        <div className="col-lg-12 ms-4">
          <h2 className='text-center mt-5 carouselTitle'>Why Hogwarts Academy?</h2>
        </div>
        <div className="row mb-5">
          <div className="col-1 buttonsCarousel">
            <a className="btn btn-outline-info ms-5" href="#carouselExampleIndicators2" role="button" data-slide="prev">←</a>
          </div>
          <div className="col-10 ms-5 mt-5 mb-5">
            <div id="carouselExampleIndicators2" className="carousel slide" data-ride="carousel">
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <div className="row">

                    <div className="col-md-4 ">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.trishmcfarlane.com/wp-content/uploads/sites/4/2015/01/technology.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Innovation</h4>
                          <p className="card-text">Inventive research-based methods of teaching and learning.</p>

                        </div>

                      </div>
                    </div>
                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.betterup.com/hubfs/Google%20Drive%20Integration/Delivery%20URL%20-%20BetterUp%20-%20importance%20of%20teamwork%20%5BARTICLE%5D-3.jpeg"/>
                        <div className="card-body">
                          <h4 className="card-title">Team work</h4>
                          <p className="card-text">Collaborate with other motivated learners on various exciting projects.</p>

                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://elearningindustry.com/wp-content/uploads/2020/06/marketing-leadership-training-programs-features.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Proactivity</h4>
                          <p className="card-text">Expect to be challenged to participate, because that is the best way to grow!.</p>

                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div className="carousel-item">
                  <div className="row">

                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://media.istockphoto.com/photos/concept-of-soft-skills-mind-map-in-handwritten-style-picture-id1341328946?k=20&m=1341328946&s=612x612&w=0&h=qbWMmzlwy9tWq59_rbKMGoaJN8Oi5W6hVAr1B87tzvc="/>
                        <div className="card-body">
                          <h4 className="card-title">Soft skills</h4>
                          <p className="card-text">Improve your interpersonal skillset, along with your technical knowledge.</p>

                        </div>

                      </div>
                    </div>
                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.freshbooks.com/blog/wp-content/uploads/2017/08/collaboration-1-852x568.jpg.optimal.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Support</h4>
                          <p className="card-text">Struggling? Our trainers and mentors are always ready to help.</p>

                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.codingdojo.com/blog/wp-content/uploads/F7DEG2ALB225H2RKE6PD754O4E.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Practical application</h4>
                          <p className="card-text">Real-life projects to apply your knowledge in practice.</p>

                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div className="carousel-item">
                  <div className="row">

                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.thehealthy.com/wp-content/uploads/2020/12/GettyImages-1207314090.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Positivity</h4>
                          <p className="card-text">Friendly environment and positive atmosphere to make you feel at home.</p>

                        </div>

                      </div>
                    </div>
                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.quotemaster.org/images/a2/a2ded292e0f7f76194bf77e401dacf5a.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Improvement</h4>
                          <p className="card-text">Enhance your workflow and analytical thinking through specially designed exercises.</p>

                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mb-3">
                      <div className="card">
                        <img className="img-fluid" alt="100%x280" src="https://www.waseda.jp/top/en/assets/uploads/2020/11/shutterstock_1676998306-2000x1333.jpg"/>
                        <div className="card-body">
                          <h4 className="card-title">Entirely online</h4>
                          <p className="card-text">Learn and practice from anywhere in the world with our web application!.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-1 buttonsCarousel'>
            <a className="btn btn-outline-info" href="#carouselExampleIndicators2" role="button" data-slide="next">
            →
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Carousel;
