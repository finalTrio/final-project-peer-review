/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import AceEditor from 'react-ace';

import 'ace-builds/src-noconflict/theme-xcode';
// import 'ace-builds/src-noconflict/mode-javascript'; // TODO when mode is added font isn't monospace

import './Console.css';
import { fromConsoleToCode } from '../../../helpers/codeParser';

const Console = ({run, args, setTab}) => {
  const [val, setVal] = useState(`function solution(${args}) {\n\n}`);

  return (
    <div>
      <div className='consoleBody'>
        <AceEditor
          style={{width: '100%'}}
          placeholder='make a function with the name "solution"'
          mode='javascript'
          theme='xcode'
          name='console'
          onChange={setVal}
          fontSize={16}
          fontFamily={'monospace'} /* This does not get evaluated? */
          showPrintMargin={true}
          showGutter={true}
          highlightActiveLine={true}
          value={val}
          setOptions={{
            showLineNumbers: true,
            tabSize: 2,
            minLines: 30,
            maxLines: Infinity,
          }}/>
      </div>
      <div className='text-center' style={{borderTop: '1px black solid'}}>
        <button className='btn btn-success my-2' onClick={() => {run(fromConsoleToCode(val)); setTab('results');}}>Submit</button>
      </div>
    </div>
  );
};

export default Console;
