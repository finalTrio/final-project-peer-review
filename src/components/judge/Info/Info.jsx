/* eslint-disable react/prop-types */
import React from 'react';
import './Info.css';

const Info = ({description, constraints, example, results, tab, setTab}) => {

  const info = (
    <div>
      <div>
        <h3>Description:</h3>
        {description}
      </div>
      <hr />
      <div>
        <h3>Constraints:</h3>
        {constraints}
      </div>
      <hr />
      <div>
        <h3>Example:</h3>
        {example}
      </div>
    </div>
  );

  return (
    <div className='infoConsole'>
      <div>
        <a style={{cursor: 'pointer'}} className='link-dark description' onClick={() => setTab('description')}>Description &nbsp;</a>
        <a style={{cursor: 'pointer'}} className='link-success result' onClick={() => setTab('result')}>Result</a>
      </div>
      <hr />
      {tab === 'description'
        ? info
        : results.map((el, i) => <p key={i}>{`Test case: ${i + 1} - `}{el? <span className='text-success'>passed</span>: <span className='text-danger'>failed</span>}</p>)}
    </div>
  );
};

export default Info;
