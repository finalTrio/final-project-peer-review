import React, { useContext, useState } from 'react';
import AppContext from '../../../context/AppContext';
import '../EditProfile/EditProfile.css';
import { ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage';
import { storage } from '../../../config/firebase-config';
import { updateFirstname, updateLastname, updatePhoneNumber, updateUserProfilePicture } from '../../../services/user.services';
import Swal from 'sweetalert2';
const EditProfile = () => {
  const {user, userData, setContext } = useContext(AppContext);
  const [name, setName] = useState({first: '', last: '', phone: ''});
  const uploadPicture = (e) => {
    e.preventDefault();
    const file = e.target[0]?.files?.[0];

    if (!file) return alert('Please select a file!');
    const picture = storageRef(storage, `images/${userData.username}/avatar`);
    uploadBytes(picture, file)
      .then(snapshot => {
        return getDownloadURL(snapshot.ref)
          .then(url => {
            return updateUserProfilePicture(user, url)
              .then(() => {
                setContext({
                  user,
                  userData: {
                    ...userData,
                    avatarUrl: url,
                  },
                });
              });
          });
      }).catch(console.error);
    return (
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Your photo was changed successfully!',
        showConfirmButton: false,
        timer: 1000
      })
    );
  };

  const update = () => {
    return updateFirstname(user, name.first)
      .then(() => {
        updateLastname(user, name.last)
          .then(() => {
            updatePhoneNumber(user, name.phone)
              .then(() => {
                setContext({
                  user,
                  userData: {
                    ...userData,
                    firstname: name.first,
                    lastname: name.last,
                    phone: name.phone,
                  }
                });
              });
          }).then(() => {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Your information was changed successfully!',
              showConfirmButton: false,
              timer: 1000
            });
          });
      });
  };

  return (
    <div className='col py-3 profilePage'>
      {user &&
      <div className="container rounded bg-white mt-5 mb-5 innerProfile">
        <div className="row">
          <div className="col-md-3 border-right">
            <div className="d-flex flex-column align-items-center text-center p-3 py-5"><img className="rounded-circle mt-5" width="150px" src={userData.avatarUrl}/><span className="font-weight-bold">{userData.username}</span><span className="text-black-50">{userData.email}</span><span> </span></div>
            <div className="profile-bio">
              <p>
                <div className="col">
                  <h6>Change photo</h6>
                  <form style={{height: '100px'}} onSubmit={uploadPicture}>
                    <input type="file" className="form-control outline-secondary" />
                    <br></br>
                    <button className='btn btn-outline-secondary' type="submit">Submit</button>
                  </form>
                </div>
              </p>
            </div>
          </div>
          <div className="col-md-5 border-right">
            <div className="p-3 py-5">
              <div className="d-flex justify-content-between align-items-center mb-3">
                <h4 className="text-right">Profile Settings</h4>
              </div>
              <div className="row mt-2">
                <div className="col-md-6"><label className="labels">Name</label><input type="text" className="form-control" placeholder={userData.firstname}  onChange={(e) => setName({...name, first: e.target.value})}/></div>
                <div className="col-md-6"><label className="labels">Surname</label><input type="text" className="form-control" placeholder={userData.lastname} onChange={(e) => setName({...name, last: e.target.value})}/></div>
              </div>
              <div className="row mt-3">
                <div className="col-md-12"><label className="labels">Mobile Number</label><input type="text" className="form-control" placeholder={userData.phone} onChange={(e) => setName({...name, phone: e.target.value})}/></div>
                <div className="col-md-12"><label className="labels">Email ID</label><input type="text" className="form-control" placeholder={userData.email} value=""/></div>
                <div className="col-md-12"><label className="labels">Education</label><input type="text" className="form-control" placeholder="education" value=""/></div>
              </div>
              <div className="row mt-3">
                <div className="col-md-6"><label className="labels">Country</label><input type="text" className="form-control" placeholder="country" value=""/></div>
                <div className="col-md-6"><label className="labels">State/Region</label><input type="text" className="form-control" value="" placeholder="state"/></div>
              </div>
              <div className="mt-5 text-center"><button className="btn btn-primary profile-button" onClick={update} >Save Profile</button></div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="p-3 py-5">
              <div className="d-flex justify-content-between align-items-center experience"><span>Edit Experience</span><span className="border px-3 p-1 add-experience"><i className="fa fa-plus"></i>&nbsp;Experience</span></div><br/>
              <div className="col-md-12"><label className="labels">Experience in Designing</label><input type="text" className="form-control" placeholder="experience" value=""/></div> <br/>
              <div className="col-md-12"><label className="labels">Additional Details</label><input type="text" className="form-control" placeholder="additional details" value=""/></div>
            </div>
          </div>
        </div>
      </div>}
    </div>
  );
};

export default EditProfile;
