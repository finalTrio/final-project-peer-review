import React, { useContext, useState } from 'react';
import AppContext from '../../../context/AppContext';
import {addPost} from '../../../services/posts.services';
import Loading from '../../Loading/Loading';
import '../CreatePost/CreatePost.css';
import Swal from 'sweetalert2';
// import { useNavigate } from 'react-router-dom';

const CreatePost = () => {
  const [info, setInfo] = useState({title:'', description:'', level: ''});
  const { user } = useContext(AppContext);
  // const navigate = useNavigate();
  const createPost = () => {
    if (!info.title || !info.description)  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please add title/description!',
      });
      return;
    }
    // if (info.level !== 'Beginner' || info.level !== 'Intermediate' || info.level !== 'Advanced')  {
    //   Swal.fire({
    //     icon: 'error',
    //     title: 'Oops...',
    //     text: 'Please choose level!',
    //   });
    //   return;
    // }

    addPost(info, user)
      .then(() => {
        setInfo({title:'', description:'', level: ''});
        // navigate('/dashboard');
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Post added successfully!',
          showConfirmButton: false,
          timer: 1500
        });
      })
      .catch(console.error);
  };

  return (
    <div className="modal fade " id="createPost">
      {!user ? <Loading /> : <div className="modal-dialog modal-lg">
        <div className="modal-content d-flex align-items-center">
          <div className="modal-header">
            <h1 className="">Create post</h1>
          </div>

          <div className="container">
            <div className="form-group">
              <label htmlFor="title">Title <span className="require">*</span></label>
              <input type="text" className="form-control" name="title" value={info.title} onChange={e => setInfo({...info, title: e.target.value})} />
            </div>
            <div className='form-group mt-3'>
              <label htmlFor="level">Level<span className="require">*&nbsp;</span></label>
              <select name="lebel" className="custom-select" value={info.level} onChange={e => setInfo({...info, level: e.target.value})}>
                <option>choose..</option>
                <option>Beginner</option>
                <option>Intermediate</option>
                <option>Advanced</option> style
              </select>
            </div>
            <div className="form-group mt-3">
              <label htmlFor="description">Description <span className="require">*</span></label>
              <textarea rows="5" className="form-control" name="description" value={info.description} onChange={e => setInfo({...info, description: e.target.value})} ></textarea>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-outline-danger" data-bs-dismiss="modal">Discard post</button>
            <button type="button" className="btn btn-outline-info" data-bs-dismiss="modal" onClick={createPost}>Create post</button>
          </div>
        </div>
      </div>}
    </div>
  );
};

export default CreatePost;
