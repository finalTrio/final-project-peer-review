/* eslint-disable react/prop-types */

//Libraries
import React, {useContext, useState} from 'react';
import * as Icons from 'react-bootstrap-icons';
import { Link, NavLink } from 'react-router-dom';

//Components
import CalendarSide from '../Calendar/CalendarSide';

//CSS
import './SideBar.css';
import { sideBarIconSize } from '../../../constants/constants';
import AppContext from '../../../context/AppContext';

const Sidebar = ({visibility}) => {
  const [mini, setMini] = useState(visibility);
  const {user, userData} = useContext(AppContext);

  return (
    <div className={mini===false ? 'col-auto col-md-4 col-xl-2 px-sm-1 px-0 bg-dark': 'col-1 px-sm-1 px-0 bg-dark mini-side'}>
      <div className='sticky-top mb-2 sideContent'>
        <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
          <button className='align-items-right sidebarToggleBtn' onClick={()=>setMini(!mini)}>{mini? <Icons.ChevronRight/> : <Icons.ChevronLeft/>}</button>
          <NavLink to="/dashboard" className="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
            <span className="fs-5 d-none d-sm-inline">{mini===false && 'Menu'} &nbsp;</span>
          </NavLink>
          <ul className="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
            <li className="nav-item">
              <NavLink to='/dashboard'className="nav-link align-middle px-0">
                <Icons.HouseDoor size={sideBarIconSize} color='#eeeeee'/><span className="ms-1 d-none d-sm-inline">{mini===false && 'Home'}</span>
              </NavLink>
            </li>
            {/* <li>
              <a href="#submenu1" data-bs-toggle="collapse" className="nav-link px-0 align-middle">
                <Icons.Pencil size={sideBarIconSize} color='#eeeeee'/><span className="ms-1 d-none d-sm-inline">{mini===false && 'Exams'}</span> </a>
              {mini===false && <ul className="collapse nav flex-column ms-1" id="submenu1" data-bs-parent="#menu">
                <li className="w-100">
                  <a href="#" className="nav-link px-0"> <span className="d-none d-sm-inline">Item</span> 1 </a>
                </li>
                <li>
                  <a href="#" className="nav-link px-0"> <span className="d-none d-sm-inline">Item</span> 2 </a>
                </li>
              </ul>}
            </li> */}
            <li>
              <a href="#submenu2" data-bs-toggle="collapse" className="nav-link px-0 align-middle ">
                <Icons.ListTask size={sideBarIconSize} color='#eeeeee'/> <span className="ms-1 d-none d-sm-inline">{mini===false && 'Tasks'}</span></a>
              {mini===false && <ul className="collapse nav flex-column ms-1" id="submenu2" data-bs-parent="#menu">
                {!user ? 'No user' : userData.role === 'student' || userData.role === 'mentor' ?  <ul>
                  <li className="w-100">
                    <NavLink to='/tasks' className="nav-link px-0"> <span className="d-none d-sm-inline">All tasks</span></NavLink>
                  </li>
                  <li className="w-100">
                    <NavLink to='/exams' className="nav-link px-0"> <span className="d-none d-sm-inline">All workshops</span></NavLink>
                  </li>
                </ul> :
                  <ul>
                    <li className="w-100">
                      <NavLink to='/create-task' className="nav-link px-0"> <span className="d-none d-sm-inline">Create tasks</span></NavLink>
                    </li>
                    <li>
                      <NavLink to='/tasks' className="nav-link px-0"> <span className="d-none d-sm-inline">All tasks</span></NavLink>
                    </li>
                    <li>
                      <NavLink to='/create-workshop' className="nav-link px-0"> <span className="d-none d-sm-inline">Create a workshop</span></NavLink>
                    </li>
                  </ul>}
              </ul>}
            </li>
            <li>
              <a href="#submenu3" data-bs-toggle="collapse" className="nav-link px-0 align-middle ">
                <Icons.Terminal size={sideBarIconSize} color='#eeeeee'/> <span className="ms-1 d-none d-sm-inline">{mini===false && 'Teams'}</span></a>
              {mini===false && <ul className="collapse nav flex-column ms-1" id="submenu3" data-bs-parent="#menu">
                {!user ? 'No user' : userData.role === 'student' || userData.role === 'mentor' ?   <li>
                  <NavLink to='/teams' className="nav-link px-0"> <span className="d-none d-sm-inline">All teams</span></NavLink>
                </li> :
                  <ul>
                    <li>
                      <li className="w-100">
                        <NavLink to='/teams/create-team' className="nav-link px-0"> <span className="d-none d-sm-inline">Create team</span></NavLink>
                      </li>
                      <li className="w-100">
                        <NavLink to='/teams' className="nav-link px-0"> <span className="d-none d-sm-inline">All teams</span></NavLink>
                      </li>
                    </li>
                  </ul>}
              </ul>}
            </li>
            <li>
              <Link to='/forum' className="nav-link px-0 align-middle">
                <Icons.CardText size={sideBarIconSize} color='#eeeeee'/> <span className="ms-1 d-none d-sm-inline">{mini===false && 'Forum'}</span> </Link>
            </li>
            {mini===false && <li className="nav-item mt-3" style={{marginLeft:'-15px'}}>
              <CalendarSide />
            </li>}
          </ul>
          <hr/>
        </div>
      </div>
    </div>
  );
};
export default Sidebar;
