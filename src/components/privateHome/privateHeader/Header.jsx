
//Libraries
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import AppContext from '../../../context/AppContext';
import { logoutUser } from '../../../services/auth.services';
import Loading from '../../Loading/Loading';
//Components
//CSS
import './Header.css';
import logo from'../../../images/logo.png';
import { Link } from 'react-router-dom';
const Header = () => {
  const {user, userData} = useContext(AppContext);
  const navigate = useNavigate();
  const logOut = (e) => {
    e.preventDefault();
    navigate('/home');
    logoutUser();
  };
  return (
    <div className='container-fluid sticky-top headerStudent'>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"></link>
      <div className='row'>
        <div className='col mt-2'>
          <Link to='/dashboard'><img src={logo} className='logo'></img></Link>
        </div>
        <div className='col'>
          <div className="dropdown text-end">
            {!user ? <Loading /> :
              <><a className="text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                <span className="d-none d-sm-inline mx-1 mt-3">{userData.firstname} {userData.lastname} </span>
                <img src={userData.avatarUrl} alt="hugenerd" width="30" height="30" className="rounded-circle mt-3" />
              </a><ul className="dropdown-menu dropdown-menu-dark text-small shadow mt-4" style={{backgroundColor: '#0c4896'}} aria-labelledby="dropdownUser1">
                <li><Link to='/edit-profile' className="dropdown-item" href="#">Settings</Link></li>
                <li><a className="dropdown-item" href="#">Profile</a></li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li><button className="dropdown-item" onClick={logOut}>Sign out</button></li>
              </ul></>
            }
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
