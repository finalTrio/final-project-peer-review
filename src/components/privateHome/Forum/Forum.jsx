/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-key */
import React, { useContext, useEffect, useState } from 'react';
import { deletePost, dislikePost, fromPostsDocument, getAllPosts, getLivePosts, likePost } from '../../../services/posts.services';
import CreatePost from '../CreatePost/CreatePost';
import PostView from '../PostView/PostView';
import '../Forum/Forum.css';
import AppContext from '../../../context/AppContext';
import Loading from '../../Loading/Loading';
const Forum = () => {
  const [posts, setPosts] = useState([]);
  const {user} = useContext(AppContext);
  const [filtered, setFiltered] = useState('Beginner');
  const [sorter, setSorter] = useState('Newest');
  const [sliced, setSliced] = useState([]);
  const like = (post) => {
    likePost(user, post.id)
      .then(() => {
        setPosts(posts.map(p => {
          if (p.id === post.id) {
            p.likedBy.push(user);
          }
          return p;
        }));
      });
  };

  const dislike = (post) => {
    dislikePost(user, post.id)
      .then(() => {
        setPosts(posts.map(t => {
          if (t.id === post.id) {
            t.likedBy = t.likedBy.filter(h => h !== user);
          }
          return t;
        }));
      });
  };

  const remove = (id) => {
    deletePost(id)
      .then(() => {
        setPosts(posts.filter(t => t.id !== id));
      })
      .catch(console.error);
  };

  useEffect(() => {
    getAllPosts()
      .then((post) => {
        setPosts([...post]);
        setSliced([...post].slice(0, 4));
      })
      .catch(console.error);
  }, []);
  useEffect(() => {
    const unsubscribe = getLivePosts((snapshot) => {
      if (snapshot.exists()) setPosts(fromPostsDocument(snapshot));
    });
    return () => unsubscribe();
  }, []);

  return (
    <div className="col py-3">
      <div className="row ps-3">
        <div className="col-9">
          <div className="row text-left mb-5">
            <div className="col-lg-6 mb-3 mb-sm-0">
              <div className="dropdown bootstrap-select form-control form-control-lg bg-white bg-op-9 text-sm w-lg-50">
                <select className="form-control form-control-lg bg-white bg-op-9 text-sm w-lg-50" data-toggle="select" tabIndex="-98" onChange={(e) => setFiltered(e.target.value)}>
                  <option> Beginner </option>
                  <option> Intermediate </option>
                  <option> Advanced </option> style
                </select>
              </div>
            </div>
            <div className="col-lg-6 text-lg-right">
              <div className="dropdown bootstrap-select form-control form-control-lg bg-white bg-op-9 ml-auto text-sm w-lg-50">
                <select className="form-control form-control-lg bg-white bg-op-9 ml-auto text-sm w-lg-50" data-toggle="select" tabIndex="-98" onChange={(e) => setSorter(e.target.value)}>
                  <option> Newest </option>
                  <option> Oldest </option>
                  <option> Top </option>
                </select>
              </div>
            </div>
          </div>
          {!posts
            ? <h1>No Posts</h1>
            : posts
              .filter((post) => post.info.level === filtered)
              .sort((a, b) => {if (sorter === 'Top') return b.likedBy.length - a.likedBy.length; else if(sorter === 'Newest') return b.createdOn - a.createdOn; else if(sorter === 'Oldest') return a.createdOn - b.createdOn;})
              .map((post) => {
                return <PostView key={post.id} post={post} like={like} dislike={dislike} />;
              })
          }
        </div>

        <div className="col-3">
          <div className="sticky"><div className="sticky-inner">
            <button className="btn btn-lg btn-block btn-success rounded-0 py-4 mb-3 bg-op-6 roboto-bold" data-bs-toggle="modal" data-bs-target="#createPost">Ask Question</button>
            <div className="bg-white mb-3">
              <h4 className="px-3 py-4 op-5 m-0">
                Active Topics
              </h4>
              {!sliced ? <Loading/> : sliced.map((post) => {
                return (
                  <div className="pos-relative px-3 py-3">
                    <h6 className="text-primary text-sm">
                      <a href="#" className="text-primary">{post.info.title}</a>
                    </h6>
                    <p className="mb-0 text-sm"><span className="op-6">Posted</span>on<a className="text-black" href="#">{new Date(post.createdOn).toLocaleDateString()}</a> <span className="op-6"></span> <a className="text-black" href="#">{post.info.description}</a></p>
                  </div>
                );
              })}
              <hr className="m-0"/>
            </div>
          </div></div>
        </div>
      </div>
      <CreatePost />
    </div>
  );
};

export default Forum;
