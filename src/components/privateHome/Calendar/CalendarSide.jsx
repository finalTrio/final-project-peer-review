import React from 'react';
import { Fragment } from 'react';
import { useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import './CalendarSide.css';

const CalendarSide = () => {
  const [value, onChange] = useState(new Date());
  return (
    <Fragment>
      <div>
        <Calendar onChange={onChange} value={value} />
      </div><p className='bold'>
        <span className='bold'>Selected Date:</span>{' '}
        {value.toDateString()}
      </p>
    </Fragment>
  );
};

export default CalendarSide;
