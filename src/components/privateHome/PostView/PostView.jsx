/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react';
import AppContext from '../../../context/AppContext';
import { getCommentsByPostId } from '../../../services/comment.services';
import CommentsList from './Comments/CommentsList';
import './PostView.css';
import * as Icon from 'react-bootstrap-icons';
const PostView = ({ post, like, dislike }) => {
  const { user } = useContext(AppContext);
  const length = post.info.description.length;
  const description = post.info.description.slice(0, 144);
  const sliced = post.info.description.slice(144);
  const isPostLiked = () => post.likedBy.includes(user);
  const [showComments, setShowComments] = useState(false);
  const [postComments, setPostComments] = useState([]);
  const [rest, setShowRest] = useState(false);

  useEffect(() => {
    getCommentsByPostId(post.id)
      .then(setPostComments)
      .catch(console.error);
  }, []);

  return (
    <div className="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
      <div className="row">
        <div className="col-md-8 mb-3 mb-sm-0">
          <h5>
            <a className="text-primary">{post.info.title}</a>
            <a className="text-secondary"> #{post.info.level}</a>
          </h5>
          <p className="text-sm"><span className="op-6">Posted on</span> <a className="text-secondary">{new Date(post.createdOn).toLocaleDateString()}</a> <span className="op-6"> by</span> <a className="text-primary">{post.author}</a></p>
          <p className='text-sm'><a className="text-black mr-2">{length <= 144 ? post.info.description : <span>{description}{<span>{rest ? <p>{sliced}<span className='text-secondary' style={{ cursor: 'pointer' }} onClick={() => setShowRest(false)}>&nbsp;show less</span></p> : <span style={{ cursor: 'pointer' }} className='text-secondary' onClick={() => setShowRest(true)}>&nbsp;see more...</span>}</span>}</span>}</a></p>
        </div>
        <div className="col op-7">
          <div className="row op-7 align-items-center">
            <div className='col px-1 mt-2'> {user && <span className='badge butt text-center forSec hovertext' data-hover={isPostLiked() ? 'dislike' : 'like'} onClick={isPostLiked() ? () => dislike(post) : () => like(post)}>{isPostLiked() ? <span className="d-block text-sm">{post.likedBy.length}{post.likedBy.length === 1 ? 'Like' : 'Likes'} {<Icon.BalloonHeartFill />}</span> : <span className="d-block text-sm">{post.likedBy.length}{post.likedBy.length === 1 ? 'Like' : 'Likes'}{<Icon.BalloonHeart />}</span>}</span>}</div>
            <div className='col px-1 mt-2'> <span className='badge butt forSec hovertext' data-hover='Reply' onClick={() => setShowComments(!showComments)}><span className="d-block text-sm">{postComments.length} {postComments.length === 1 ? 'Reply' : 'Replies'}</span></span> </div>
            <div className='col px-1 mt-2'> <span className='badge butt text-center forSec hovertext' data-hover='Views'><span className="d-block text-sm">New<Icon.Person /></span></span></div>
          </div>
        </div>
      </div>
      <div className="comments" style={{ marginTop: '-20px' }}>{user && showComments ?
        <CommentsList comments={postComments} setComments={setPostComments} post={post} /> : null}</div>
    </div>
  );
};

export default PostView;
{/* <div className='col'>
<div className="container"> <a className="text-black mr-2">{length <= 144 ? post.info.description : <span>{description}{<span>{rest ? <p>{sliced}<span className='text-secondary' style={{cursor: 'pointer'}} onClick={() => setShowRest(false)}>&nbsp;show less</span></p> : <span style={{cursor: 'pointer'}} className='text-secondary' onClick={() => setShowRest(true)}>&nbsp;see more...</span>}</span>}</span>}</a></div>
</div> */}
