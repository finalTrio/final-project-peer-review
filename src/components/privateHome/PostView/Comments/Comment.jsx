/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useEffect } from 'react';
import { getUserByUsername } from '../../../../services/user.services';

const Comment = ({comment}) => {
  const [pic, setPic] = useState();

  useEffect(() => {
    getUserByUsername(comment.author)
      .then(user => setPic(user.avatarUrl));
  }, []);


  return (
    <div className="card p-3 mt-2">
      <div className="d-flex justify-content-between align-items-center">
        <div className="user d-flex flex-row align-items-center">
          <img src={pic} width="30" className="user-img rounded-circle mr-2"/>
          <span><small className="font-weight-bold text-primary">{comment.author}</small></span>
        </div>
        <small>{new Date(comment.createdOn).toLocaleDateString()}</small>
      </div>

      <div className="action d-flex justify-content-between mt-2 align-items-center">
        <div className="reply px-4">
          <span>{comment.body}</span>
        </div>
      </div>
    </div>

  );
};

export default Comment;


