/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react';
import AppContext from '../../../../context/AppContext';
import { addComment, commentPost, fromCommentsCollection, getCommentsByPostId, getLiveComments } from '../../../../services/comment.services';
import Comment from './Comment';
import CommentForm from './CommentForm';

const CommentsList = ({post, comments, setComments}) => {
  const [commentForm, showCommentForm] = useState(false);
  const {userData} = useContext(AppContext);

  useEffect(() => { // TODO add db listener
    getCommentsByPostId(post.id)
      .then(setComments)
      .catch(console.error);
  }, []);

  useEffect(() => {
    const unsubscribe = getLiveComments((snapshot) => {
      if (snapshot.exists()) setComments(fromCommentsCollection(snapshot));
    }, post.id);

    return () => unsubscribe();
  }, []);

  const createComment = (body) => {
    return addComment(body, userData.username, post.id)
      .then((comment) => {
        commentPost(comment);
        setComments([comment, ...comments]);
      })
      .catch(console.error);
  };

  return (
    <div className='container-fluid'>
      {comments.length === 0 ?
        <h5 className='text-center' style={{marginTop: '40px'}}>No replies yet</h5> :
        <div className="container mt-5">
          <div className="row  d-flex justify-content-center">
            <div className="col-md-8">
              <div className="headings d-flex justify-content-between align-items-center mb-3">
                <h5>{comments.length}Replies: </h5>
              </div>
              {comments.map((comment) =>
                <Comment key={comment.id} comment={comment} />)}
            </div>
          </div>
        </div>}
      <div className='text-center mt-2 mb-2 create-comment'>
        <div><span className='badge butt forSec mt-2 mb-2' onClick={() => showCommentForm(!commentForm)}>Reply</span></div>
        {commentForm ?
          <CommentForm onSubmit={createComment}
            comments={comments} setComments={setComments} /> :
          null}
      </div>
    </div>
  );
};

export default CommentsList;

