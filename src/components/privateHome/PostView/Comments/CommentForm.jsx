/* eslint-disable react/prop-types */
import React, {  useContext, useState } from 'react';
import AppContext from '../../../../context/AppContext';
import './CommentForm.css';
const CommentForm = ({onSubmit}) => {
  const [body, setBody] = useState('');
  const {user, userData} = useContext(AppContext);

  const createComment = (e) => {
    e.preventDefault();
    onSubmit(body)
      .then(() => {
        setBody('');
      })
      .catch(console.error);
  };

  return (
    <div className="container">
      <div className="row d-flex justify-content-center">
        <div className="col-1">
          {user && <img src={userData.avatarUrl ? userData.avatarUrl : 'https://img.lovepik.com/free-png/20210926/lovepik-cartoon-avatar-png-image_401440477_wh1200.png'} width="50" className="rounded-circle mt-2"/>}
        </div>
        <div className="col-8">
          <div className="comment-box ml-2">
            <div className="comment-area">
              <span><textarea className="form-control" placeholder="what is your view?" rows="4" value={body} onChange={(e) => setBody(e.target.value)}></textarea></span>
            </div>
            <div className="comment-btns mt-2">
              <div className="row">
                <div className="col-6">
                  <div className="pull-right">
                    <button className="btn btn-success btn-sm" type='submit' onClick={(e) => {createComment(e);}}>Post</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CommentForm;
