
//Libraries
import React, { useEffect, useState } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase-config';
import AppContext from './context/AppContext';
//Components
import Login from './views/Login/Login';
import Home from './views/Home/Home';
import NotFoundView from './views/NotFoundView';
import Register from './views/Register/Register';
import { getUserData } from './services/user.services';
import Loading from './components/Loading/Loading';
import ScrollButton from './components/ScrollButton/ScrollButton';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
//CSS
import './App.css';
import PublicPage from './components/Pages/PublicPage/PublicPage';
import StudentPage from './components/Pages/StudentPage/StudentPage';
import CreateTask from './views/CreateTask/CreateTask';
import DashBoard from './components/studentView/Dashboard/DashBoard';
import AllTasks from './components/studentView/Course/Tasks/AllTasks/AllTasks';
import Task from './views/Task/Task';
import Forum from './components/privateHome/Forum/Forum';
import EditProfile from './components/privateHome/EditProfile/EditProfile';
import TeacherPage from './components/Pages/TeacherPage/TeacherPage';
import Dashboard from './components/teacherView/Dashboard/Dashboard';
import Teams from './components/teacherView/Teams/Teams';
import AllTasksToCheck from './components/teacherView/AllTasks/AllTasksToCheck';
import CreateTeam from './components/teacherView/Teams/CreateTeam';
import AddMember from './components/teacherView/Teams/AddMember/AddMember';
import CreateWorkshop from './views/CreateWorkshop/CreateWorkshop';
import Exam from './views/Exam/Exam';
import ExamAssess from './views/ExamAssess/ExamAssess';
import AllExams from './components/studentView/Course/Exams/AllExams/AllExams';

function App() {
  let [user, loading] = useAuthState(auth);
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });
  const userInfo = appState.userData;
  useEffect(() => {
    if (user === null) return;
    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) return;
        const data = snapshot.val()[Object.keys(snapshot.val())[0]];
        setAppState({
          user: data.username,
          userData: data,
        });
      })
      .catch(console.error);
  }, [user]);

  if (loading) return <Loading />;

  return (
    <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
      <ScrollToTop>
        <Routes>
          <Route index element={<Navigate replace to="home" />} />
          <Route path='/home' element={<PublicPage><Home/></PublicPage>} />
          <Route path='/login' element={<PublicPage><Login/></PublicPage>} />
          <Route path='/register' element={<PublicPage><Register/></PublicPage>} />
          <Route path='/dashboard' element={!userInfo ? <Loading title='information'/> : userInfo.role == 'admin' || userInfo.role == 'teacher' ? <TeacherPage><Dashboard/></TeacherPage> : <StudentPage><DashBoard/></StudentPage>} />
          <Route path='/create-task' element={<StudentPage> <CreateTask/></StudentPage>}/>
          <Route path='/create-workshop' element={<StudentPage> <CreateWorkshop/></StudentPage>} />
          <Route path='/tasks' element={!userInfo ? <Loading title='information'/> : userInfo.role == 'admin' || userInfo.role == 'teacher' ? <TeacherPage><AllTasksToCheck/></TeacherPage> : <StudentPage><AllTasks/></StudentPage>} />
          <Route path='/task/:id' element={<StudentPage><Task/></StudentPage>} />
          <Route path='/exam/:title/:wid/:team/' element={<StudentPage><Exam/></StudentPage>} />
          <Route path='/exams' element={<StudentPage><AllExams /></StudentPage>} />
          <Route path='/assess/:title/:wid/:team/:user' element={<StudentPage visibility={true}><ExamAssess/></StudentPage>} />
          <Route path='/forum' element={<StudentPage visibility={true}><Forum /></StudentPage>} />
          <Route path='/edit-profile' element={<StudentPage visibility={true}><EditProfile /></StudentPage>} />
          <Route path='/teams' element={<TeacherPage><Teams /></TeacherPage>} />
          <Route path='teams/create-team' element={<TeacherPage><CreateTeam /></TeacherPage>} />
          <Route path='teams/add-members/:id' element={<TeacherPage><AddMember /></TeacherPage>} />
          <Route path="*" element={<NotFoundView />} />
        </Routes>
      </ScrollToTop>
      <ScrollButton />
    </AppContext.Provider>
  );
}

export default App;
