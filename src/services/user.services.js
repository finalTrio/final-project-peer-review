import { get, set, ref, query, equalTo, orderByChild, update } from 'firebase/database';
import { db } from '../config/firebase-config';
import { userRole } from '../common/roles';
import { programs } from '../common/programs';

export const createUser = (username, uid, email, firstname, lastname, phone) => {
  return set(ref(db, `users/${username}`), { username, uid, email, firstname, lastname, phone, role: userRole.student, program: programs.beginner, team: 'none', avatarUrl: 'https://img.lovepik.com/free-png/20210926/lovepik-cartoon-avatar-png-image_401440477_wh1200.png', createdOn: new Date()});
};

const fromUsersDocument = (snapshot) => {
  const usersDocument = snapshot.val();

  return Object.keys(usersDocument).map((key) => {
    const user = usersDocument[key];

    return {
      ...user,
      id: key,
    };
  });
};


export const getAllUsers = () => {
  return get(ref(db, 'users'))
    .then((snapshot) => {
      if (!snapshot.exists()) {
        return [];
      }

      return fromUsersDocument(snapshot);
    });
};
export const getUserByUsername =  (username) => {
  return get(ref(db, `users/${username}`))
    .then((snapshot) => {
      if (snapshot.exists()) return snapshot.val();
    });
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const getResults = (username) => {
  return get(ref(db, `users/${username}/grades`))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return snapshot.val();
    });
};


export const updateUserProfilePicture = (username, url) => {
  return update(ref(db), {
    [`users/${username}/avatarUrl`]: url,
  });
};


export const updateFirstname = (user, firstname) => {
  return update(ref(db), {
    [`users/${user}/firstname`]: firstname
  });
};

export const updateLastname = (user, lastname) => {
  return update(ref(db), {
    [`users/${user}/lastname`]: lastname
  });
};

export const updatePhoneNumber = (user, phoneNumber) => {
  return update(ref(db), {
    [`users/${user}/phone`]: phoneNumber
  });
};

export const updateTeam = (username, teamId) => {
  return update(ref(db, `users/${username}`), {team: teamId}).catch(console.error);
};
