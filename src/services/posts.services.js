import { ref, push, get, query, equalTo, orderByChild, update, onValue, set } from 'firebase/database';
import { ref as sRef ,deleteObject } from 'firebase/storage';
import { db, storage } from '../config/firebase-config';


export const fromPostsDocument = snapshot => {
  const postsDocument = snapshot.val();

  return Object.keys(postsDocument).map(key => {
    const post = postsDocument[key];
    return {
      ...post,
      id: key,
      createdOn: new Date(post.createdOn),
      likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
      comments: post.comments ? Object.keys(post.comments) : []
    };
  });
};

export const addPost = (info, username) => {

  return push(
    ref(db, 'posts'),
    {
      info: {...info},
      author: username,
      views: 0,
      createdOn: Date.now(),
    },
  )
    .then(result => {
      return result.key;
    });
};

export const updatePost = (id, content) => {

  return update(
    ref(db, `posts/${id}`), {content});
};

export const getPostById = (id) => {

  return get(ref(db, `posts/${id}`))
    .then(result => {
      if (!result.exists()) {
        throw new Error(`Post with id ${id} does not exist!`);
      }

      const post = result.val();
      post.id = id;
      post.createdOn = new Date(post.createdOn);
      if (!post.likedBy) {
        post.likedBy = [];
      } else {
        post.likedBy = Object.keys(post.likedBy);
      }

      return post;
    });
};

export const getLikedPosts = (username) => {

  return get(ref(db, `users/${username}`))
    .then(snapshot => {
      if (!snapshot.val()) {
        throw new Error(`User with username ${username} does not exist!`);
      }

      const user = snapshot.val();
      if (!user.likedPosts) return [];

      return Promise.all(Object.keys(user.likedPosts).map(key => {

        return get(ref(db, `posts/${key}`))
          .then(snapshot => {
            const post = snapshot.val();
            if (snapshot) {
              return {
                ...post,
                id: key,
                createdOn: new Date(post.createdOn),
                likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
              };
            }
          });
      }));
    });
};

export const getPostsByAuthor = (username) => {

  return get(query(ref(db, 'posts'), orderByChild('author'), equalTo(username)))
    .then(snapshot => {
      if (!snapshot.exists()) return [];

      return fromPostsDocument(snapshot);
    });
};

export const getAllPosts = () => {

  return get(ref(db, 'posts'))
    .then (snapshot => {
      if (!snapshot.exists()) return [];

      return fromPostsDocument(snapshot);
    });
};

export const likePost = (username, postId) => {
  const updateLikes = {};
  updateLikes[`/posts/${postId}/likedBy/${username}`] = true;
  updateLikes[`/users/${username}/likedPosts/${postId}`] = true;

  return update(ref(db), updateLikes);
};

export const dislikePost = (username, postId) => {
  const updateLikes = {};
  updateLikes[`/posts/${postId}/likedBy/${username}`] = null;
  updateLikes[`/users/${username}/likedPosts/${postId}`] = null;

  return update(ref(db), updateLikes);
};

export const deletePost = async (id) => {
  const post = await getPostById(id);
  const updateLikes = {};

  deleteObject(sRef(storage, `post-images/${id}`));

  post.likedBy.forEach(username => {
    updateLikes[`/users/${username}/likedTweets/${id}`] = null;
  });

  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/posts/${id}`]: null,
  });
};

export const getLivePosts = (listen) => {
  return onValue(ref(db, 'posts'), listen);
};

export const updateComments = (postId, comment) => {
  return set(ref(db, `/posts/${postId}/comments`), {...comment});
};

export const getComments = (postId) => {
  return get(ref(db, `/posts/${postId}/comments`))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return snapshot.val();
    });
};
