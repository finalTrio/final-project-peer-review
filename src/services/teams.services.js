import { ref, push, get, set} from 'firebase/database';
import { db } from '../config/firebase-config';
import { updateTeam } from './user.services';

export const fromTeamsDocument = snapshot => {
  const teamDocument = snapshot.val();

  return Object.keys(teamDocument).map(key => {
    const team = teamDocument[key];
    return {
      ...team,
      id: key,
    };
  });
};

export const createTeam = (info) => {

  return push(
    ref(db, 'teams'),
    {
      name: info.name,
      teacher: info.teacher,
      members: info.members || [],
    },
  )
    .then(result => {
      info.members.forEach((member) => {
        updateTeam(member, result.key);
      });
      return result.key;
    });
};

export const updateMembers = (teamId, people) => {
  return set(ref(db, `teams/${teamId}/members`), people);
};


export const getAllTeams =  () => {
  return get(ref(db, 'teams'))
    .then((snapshot) => {
      if (!snapshot.exists()) {
        return [];
      }
      return fromTeamsDocument(snapshot);
    });
};


export const getTeamById = (id) => {
  return get(ref(db, `teams/${id}`))
    .then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.val();
      }
    });
};

