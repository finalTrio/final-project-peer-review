import { get, set, ref, push } from 'firebase/database';
import { ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage';
import { db, storage } from '../config/firebase-config';
import { saveAs } from 'file-saver';

const fromWorkshopDocument = (snapshot) => {
  const workshopDocument = snapshot.val();

  return Object.keys(workshopDocument).map((key) => {
    const workshop = workshopDocument[key];

    return {
      ...workshop,
      id: key,
      createdOn: new Date(workshop.createdOn),
    };
  });
};

const fromSolutionDocument = (snapshot) => {
  const solutionDocument = snapshot.val();

  return Object.keys(solutionDocument).map((key) => {
    const url = solutionDocument[key];

    return {
      url: url,
      id: key, // id = username's solution
    };
  });
};

const fromCasesDocument = (snapshot) => {
  const solutionDocument = snapshot.val();

  return Object.keys(solutionDocument).map((key) => {
    const [description, points] = solutionDocument[key].split(':').map(el => el.trim());
    return {
      description: description,
      points: Number(points),
      id: key,
    };
  });
};

const fromResultsDocument = (snapshot) => {
  const resultDocument = snapshot.val();

  return Object.keys(resultDocument).map((key) => {
    const result = resultDocument[key];

    return {
      ...result, // {feedback: "", result: 5}
      id: key,
    };
  });
};


export const uploadWorkshop = (name, teams, cases, file) => {
  return push(ref(db, 'workshops'), {name: name, teams: teams, cases: cases, file: null, submissions: null, createdOn: Date.now()})
    .then((result) => {
      uploadBytes(storageRef(storage, `workshops/${result.key}/file/description.zip`), file)
        .then((snapshot) => {
          getDownloadURL(snapshot.ref)
            .then((url) => {
              return set(ref(db, `workshops/${result.key}/file`), url);
            });
        })
        .catch(console.error);
    });
};

export const getAllWorkshops = () => {
  return get(ref(db, 'workshops'))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return fromWorkshopDocument(snapshot);
    });
};

export const getWorkshopByID = (tid) => {
  return get(ref(db, `workshops/${tid}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return null;
      return snapshot.val();
    });
};

export const getWorkshopDescriptionByID = (wid) => {
  return get(ref(db, `workshops/${wid}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      saveAs(snapshot.val().file, `${snapshot.val().name}-solution.zip`);
    });
};

export const uploadSolution = (team, wid, username, file) => {
  return uploadBytes(storageRef(storage, `workshops/${wid}/${team}/${username}/solution.zip`), file)
    .then(snapshot => {
      getDownloadURL(snapshot.ref)
        .then(url => {
          return set(ref(db, `workshops/${wid}/submissions/${team}/${username}`), url);
        });
    })
    .catch(console.error);
};

export const getSolutions = (team, wid) => {
  return get(ref(db, `workshops/${wid}/submissions/${team}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return fromSolutionDocument(snapshot);
    });
};

export const getSolution = (team, wid, username) => {
  return get(ref(db, `workshops/${wid}/submissions/${team}/${username}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return null;
      saveAs(snapshot.val(), 'solution.zip');
    });
};

export const getCases = (wid) => {
  return get(ref(db, `workshops/${wid}/cases`))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return fromCasesDocument(snapshot);
    });
};

export const uploadResult = (team, wid, username, assessor, data) => {
  return set(ref(db, `workshops/${wid}/results/${team}/${username}/${assessor}`), data)
    .then((result) => {
      return result.key;
    });
};

export const getResults = (team, wid, username) => {
  return get(ref(db, `workshops/${wid}/results/${team}/${username}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return fromResultsDocument(snapshot);
    });
};
