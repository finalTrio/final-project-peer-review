
import { equalTo, get, onValue, orderByChild, push, query, ref, update } from 'firebase/database';
import { db } from '../config/firebase-config';

export const fromCommentsCollection = (snapshot) => {
  const commentsCollection = snapshot.val();

  return Object.keys(commentsCollection).map((key) => {
    const comment = commentsCollection[key];

    return {
      ...comment,
      id: key,
      createdOn: new Date(comment.createdOn),
    };
  });
};


export const getPostComments = (postId) => {
  return get(ref(db, `posts/${postId}/comments`))
    .then((snapshot) => {
      if (!snapshot.exists()) {
        return [];
      }

      return fromCommentsCollection(snapshot);
    });
};

export const addComment = (body, username, postId) => {
  const newComment = {
    body,
    author: username,
    createdOn: Date.now(),
    postId: postId,
  };
  return push(
    ref(db, 'comments'), newComment)
    .then((result) => {
      return ({...newComment, id: result.key});
    });
};

export const getCommentsByPostId = (postId) => {
  return get(query(ref(db, 'comments'),
    orderByChild('postId'), equalTo(postId)))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];

      return fromCommentsCollection(snapshot);
    });
};

export const getAllComments = () => {
  return get(ref(db, 'comments'))
    .then((snapshot) => {
      if (!snapshot.exists()) {
        return [];
      }

      return fromCommentsCollection(snapshot);
    });
};

export const commentPost = (comment) => {
  const updateComments = {};
  updateComments[`/posts/${comment.postId}/comments/${comment.id}`] = comment;

  return update(ref(db), updateComments);
};

export const getLiveComments = (listen, pid) => {
  return onValue(ref(db, `posts/${pid}/comments`), listen);
};
