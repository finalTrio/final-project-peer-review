import { get, set, ref, push } from 'firebase/database';
import { db } from '../config/firebase-config';

const fromTasksDocument = (snapshot) => {
  const tasksDocument = snapshot.val();

  return Object.keys(tasksDocument).map((key) => {
    const task = tasksDocument[key];

    return {
      ...task,
      id: key,
      createdOn: new Date(task.createdOn),
    };
  });
};

export const uploadTask = (data) => {
  return push(ref(db, 'tasks'), {...data, solutions: null, createdOn: Date.now()})
    .then(result => result.key);
};

export const getAllTasks = () => {
  return get(ref(db, 'tasks'))
    .then((snapshot) => {
      if (!snapshot.exists()) {
        return [];
      }

      return fromTasksDocument(snapshot);
    });
};

export const getTaskByID = (tid) => {
  return get(ref(db, `tasks/${tid}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return null;
      return snapshot.val();
    });
};

export const uploadSolution = (username, tid, result) => {
  return set(ref(db, `tasks/${tid}/solutions/${username}`), result);
};

export const getResult = (username, tid) => {
  return get(ref(db, `tasks/${tid}/result/${username}`))
    .then((snapshot) => {
      if (!snapshot.exists()) return null;
      return snapshot.val();
    });
};
