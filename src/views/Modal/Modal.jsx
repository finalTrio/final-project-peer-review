/* eslint-disable react/prop-types */
import React from 'react';
// import { useNavigate } from 'react-router';
import programInfo from '../../common/programInfo';
import './Modal.css';

//TODO Finish css transform
const Modal = () => {
  // const navigate = useNavigate();
  return (
    <div className=''>
      <div className="modal fade" id="myModal1">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">

            <div className="modal-header">
              <h4 className="modal-title">{programInfo[1].header}</h4>
              <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
            </div>

            <div className="modal-body">
              <div className='row'>
                <div className='col'>
                  <img src={programInfo[1].image} style={{ width: '80%', marginLeft: '5%' }} />
                </div>
                <div className='col me-2  box-text'>
                  <h2>{programInfo[1].title}</h2>
                  <p style={{ fontSize: '20px', color: ' #7db6e7' }}>{programInfo[1].body}</p>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              {/* <button type="button" className="apply btn" onClick={()=>navigate('/register')}>Apply</button> */}
            </div>
          </div>
        </div>
      </div>
      <div className="modal fade" id="myModal2">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">

            <div className="modal-header">
              <h4 className="modal-title">{programInfo[2].header}</h4>
              <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
            </div>

            <div className="modal-body">
              <div className='row'>
                <div className='col text-center mt-4'>
                  <img src={programInfo[2].image} style={{ width: '80%', marginLeft: '5%'}} />
                </div>
                <div className='col me-2  box-text'>
                  <h2>{programInfo[2].title}</h2>
                  <p style={{ fontSize: '20px', color: ' #7db6e7' }}>{programInfo[2].body}</p>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              {/* <button type="button" className="apply btn" onClick={()=>navigate('/register')}>Apply</button> */}
              {/* <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button> */}
            </div>

          </div>
        </div>
      </div>
      <div className="modal fade" id="myModal3">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">

            <div className="modal-header">
              <h4 className="modal-title">{programInfo[3].header}</h4>
              <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
            </div>

            <div className="modal-body">
              <div className='row'>
                <div className='col '>
                  <img src={programInfo[3].image} style={{ width: '80%', marginLeft: '5%' }} />
                </div>
                <div className='col me-2  box-text'>
                  <h2>{programInfo[3].title}</h2>
                  <p style={{ fontSize: '20px', color: ' #7db6e7' }}>{programInfo[3].body}</p>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              {/* <button type="button" className="apply btn" onClick={navigate(()=>'/register')}>Apply</button> */}
              {/* <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button> */}
            </div>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
