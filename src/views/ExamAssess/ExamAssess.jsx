/* eslint-disable react/prop-types */

// TODO form for assessment
import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import TestCase from '../../components/studentView/Course/Exams/TestCase/TestCase';
import AppContext from '../../context/AppContext';
import { getCases, getSolution, uploadResult } from '../../services/workshop.services';
import './examAssess.css';

const ExamAssess = () => {
  const {userData} = useContext(AppContext);
  const {title, wid, team, user} = useParams();
  const [cases, setCases] = useState([]);
  const [points, setPoints] = useState([]);
  const [advice, setAdvice] = useState('');

  useEffect(() => {
    getCases(wid)
      .then((c) => {
        setCases([...c]);
        setPoints([...Array.from({length: c.length})]);
      });
  }, []);

  const submit = () => {
    if (points.includes(undefined)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please answer all the questions!',
      });
    }
    const result = points.reduce((acc, el) => acc + el ,0);
    uploadResult(team, wid, user, userData.username, {result: result, feedback: advice});
  };

  return (
    <div className='col py-3'>
      <h1 className='text-center mb-3'>{title.replace('-', ' ')} assessment</h1>
      <div className='container text-center outer'>
        <div className='row ba'>
          <div className='text-center mt-3'>
            <div className='download-assess'><h4 className=' ms-2 me-2'>Download from here: <span  className='text-primary' style={{cursor: 'pointer'}}  onClick={() => getSolution(team, wid, user)}>solution.zip</span></h4></div><br/>
            <h1 className='ms-2 me-2'> Assessment </h1>
            <div className='container-md  mt-2 mb-4'>
              <span className='how-to-use mb-4'>Description: assessment requirments, Point: markirai yes ako e okay </span>
              {cases && cases.map((el, i) => <TestCase key={i} index={i} description={el.description} casePoints={el.points} points={points} setPoints={setPoints}/>)}
              <br></br>
              <div className=''>
                <div className="form-workshop">
                  <h4 className='createCommentLabel' htmlFor="exampleFormControlTextarea1">Some advice</h4>
                  <textarea className="form-content" id="exampleFormControlTextarea1" rows="2"
                    value={advice}
                    onChange={(e)=>setAdvice(e.target.value)}></textarea>
                </div>
              </div>
            </div>
            <div>
              <button className='btn btn-success mb-3' onClick={() => submit()}>Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
// save result, assessUsername, selfUsername

// download solution YES

// cases YES

// feedback

// submit

export default ExamAssess;
