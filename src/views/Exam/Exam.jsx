import React from 'react';
import { useState, useContext, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getResults, getSolutions, getWorkshopDescriptionByID, uploadSolution } from '../../services/workshop.services';
import AppContext from '../../context/AppContext';
//import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import Loading from '../../components/Loading/Loading';


const Exam = () => {
  const { userData } = useContext(AppContext);
  const { title, wid, team } = useParams();
  const [solutions, setSolutions] = useState([]);
  const navigate = useNavigate();
  const [results, setResults] = useState([]);

  useEffect(() => {
    getSolutions(team, wid)
      .then((s) => setSolutions([...s]));
    if (userData) {
      getResults(team, wid, userData.username)
        .then((r) => setResults([...r]));
    }
  }, [userData]);

  const submitSolution = (e) => {
    e.preventDefault();
    const file = e.target[0]?.files?.[0];

    if (!file) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'File is missing!',
      });
      return;
    }
    uploadSolution(team, wid, userData.username, file)
      .then(() => {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Solution added successfully!',
          showConfirmButton: false,
          timer: 1500
        });
      });
  };
  if (!userData) return;
  return (
    <div className='col py-3'>
      <h3 className='text-center mr-5'>{title.replace('-', ' ')}</h3>
      <h3>Download description</h3>
      <a className='btn btn-primary m-2' onClick={() => getWorkshopDescriptionByID(wid)}>Get description</a>
      <hr/>
      <h3>Upload solution</h3>
      <form className='m-2' style={{height: '100px', width: '85%'}} onSubmit={(e)=> submitSolution(e)}>
        <input type="file" className="form-control outline-secondary" />
        <br></br>
        <button className='btn btn-outline-secondary' type="submit">Submit</button>
      </form>
      <hr/>
      <div className='workshop-grades'>
        <h3>Results:</h3>
        {!results? <Loading/> : results.length===0? <h5>No grades yet.</h5> :  <div>{results.map((res, i)=> <h5 key={i}>{res.result} - {res.feedback}</h5>)} <h5 className='text-primary'>Average result: {results.reduce((acc, el, _, arr) => (acc + el.result / arr.length) ,0).toFixed(2)}</h5></div>}
      </div>
      <hr/>
      <h3>solutions for assessment:</h3>
      {
        !solutions
          ? 'No solutions'
          : solutions
            .map((solution, i) => {
              return (
                <div key={solution.id}>
                  <h4>{userData.username === solution.id && <span>Your </span>}solution {i + 1}<span className='text-primary' style={{ cursor: 'pointer' }} onClick={() => navigate(`/assess/${title}/${wid}/${team}/${solution.id}`)}>Assess</span></h4>
                </div>
              );
            })
      }

    </div>
  );
};

export default Exam;

