// Libraries
import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
// Components
import programInfo from '../../common/programInfo';
// CSS
import './SingleProgram.css';
//TODO Finish css transform
const SingleProgram = ({ programId, closeWindow }) => {

  return (
    <div className='popup-box'><div className='box container-fluid'>
      <Modal.Header className='box-header' closeButton onClick={() => closeWindow()}>
        <Modal.Title className='box-title'>{programInfo[programId].header}</Modal.Title>
      </Modal.Header>

      <Modal.Body className='container-fluid mt-2'>
        <div className='row'>
          <div className='col'>
            <img src={programInfo[programId].image} style={{ width: '80%', marginLeft: '5%' }} />
          </div>
          <div className='col box-text'>
            <h2>{programInfo[programId].title}</h2>
            <p style={{ fontSize: '20px' }}>{programInfo[programId].body}</p>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer className='box-footer'>
        <Button style={{ backgroundColor: '#003c66', border: 'none' }}>Apply now</Button>
      </Modal.Footer>
    </div></div>
  );
};

SingleProgram.propTypes = {
  programId: PropTypes.string,
  closeWindow: PropTypes.func
};
export default SingleProgram;
