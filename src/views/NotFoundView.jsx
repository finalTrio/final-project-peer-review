import React from 'react';
import './NotFoundView';

const NotFoundView = () => {
  return (
    <div>Not found</div>
  );
};

export default NotFoundView;
