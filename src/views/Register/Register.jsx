import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import './Register.css';
import Swal from 'sweetalert2';
import { registerUser } from '../../services/auth.services';
import { getUserByUsername, createUser } from '../../services/user.services';

const Register = () => {
  const navigate = useNavigate();
  const [form, setForm] = useState({first: '', last: '', username: '', email: '', password: '', confirmPassword: '', phone: ''});

  const submit = (e) => {
    e.preventDefault();
    if (form.password !== form.confirmPassword) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Passwords don\'t match!'
      });
    }
    if (form.password.length < 6) {
      return(Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Password should be more than 6 symbols!',
      }));
    }

    getUserByUsername(form.username)
      .then(user => {
        if (user) {
          return(Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: `User with username @${form.username} already exists!`,
          }));
        }

        return registerUser(form.email, form.password)
          .then(u => {
            createUser(form.username, u.user.uid, u.user.email, form.first, form.last, form.phone)
              .then(() => {
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Registration was successfull!',
                  showConfirmButton: false,
                  timer: 1800
                });
                navigate('/dashboard');
              })
              .catch(console.error);
          })
          .catch(e => {
            if (e.message.includes('email-already-in-use')) {
              return(Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: `Email ${form.email} has already been registered!`,
                // footer: '<a href="">Why do I have this issue?</a>'
              }));
            }
            if (e.message.includes('invalid-email')) {
              return(Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: `Email ${form.email} is invalid!`,
              // footer: '<a href="">Why do I have this issue?</a>'
              }));
            }
          });
      })
      .catch(console.error);
  };

  return (
    <div className='container-fluid lg register-body'>
      <div className='form-image'></div>
      <div className="register-form container-fluid">
        <form className='register-form-form'onKeyUp={(e) => e.key==='Enter' && submit(e)}>
          <h1 className='register'>Register</h1>
          <div className="content">
            <div className="input-field">
              <input type="text" placeholder="First Name" autoComplete='none' value={form.first} onChange={(e) => setForm({...form, first: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="text" placeholder="Last Name" autoComplete='none' value={form.last} onChange={(e) => setForm({...form, last: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="text" placeholder="username" autoComplete='none' value={form.username} onChange={(e) => setForm({...form, username: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="email" placeholder="Email" autoComplete='none' value={form.email} onChange={(e) => setForm({...form, email: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="password" placeholder="Password" autoComplete="new-password" value={form.password} onChange={(e) => setForm({...form, password: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="password" placeholder="Confirm Password" autoComplete="new-password" value={form.confirmPassword} onChange={(e) => setForm({...form, confirmPassword: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="tel" placeholder="Phone Number" autoComplete="none" pattern="[+]{1}[0-9]{11,14}" value={form.phone} onChange={(e) => setForm({...form, phone: e.target.value})} />
            </div>
          </div>
          <div className="action mb-2">
            <button type = 'submit' onClick={(e) => submit(e)}>Sign up</button>
            <button onClick={() => navigate('/login')}>Login</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
