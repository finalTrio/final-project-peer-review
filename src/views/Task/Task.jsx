import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getTaskByID } from '../../services/task.services';
import Console from '../../components/judge/Console/Console';
import Info from '../../components/judge/Info/Info';

import * as Icons from 'react-bootstrap-icons';

import './Task.css';
import { codeRunner, createFunction } from '../../helpers/codeParser';

const Task = () => {
  const {id} = useParams();
  const [task, setTask] = useState({});
  const [results, setResults] = useState([]);
  const [tab, setTab] = useState('description');
  const navigate = useNavigate();

  useEffect(() => {
    getTaskByID(id).then(setTask);
  }, []);

  const run = (code) => {
    const fn = createFunction([...task.params.split(', ')], code);
    setResults(codeRunner(fn, task.cases));
  };

  return (
    <div className='col py-3'>
      <div className='container mt-3'>
        <div className='task-back' data-hover='Back to tasks'>
          <Icons.ArrowLeftCircle size={30} onClick={() => navigate(-1)} style={{cursor: 'pointer'}}/>
        </div>
        <h2 className='text-center mt-2'>{task.title}</h2>
        <div className='row mt-3 judgeSystem'>
          <div className='col info'>
            <Info description={task.description} constraints={task.constraints} example={task.example} results={results} tab={tab} setTab={setTab} />
          </div>
          <div className='col'>
            {Object.keys(task).length? <Console run={run} args={task.params} setTab={setTab}/>: <p>loading</p>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Task;
