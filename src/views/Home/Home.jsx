import React from 'react';
/* Components */
import video from '../../images/smiling.mp4';
import Cards from '../../components/home/Cards/Cards';
/* CSS */
import './Home.css';

const Home = () => {

  return (
    <div>
      <section className="section main-banner" id="top" data-section="section1">
        <video autoPlay muted loop id="bg-video">
          <source src={video} type="video/mp4" />
        </video>
      </section>
      <Cards />
      <div>
      </div>
    </div>
  );
};

export default Home;
