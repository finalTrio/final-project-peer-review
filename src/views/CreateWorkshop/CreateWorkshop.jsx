import { Dropdown } from 'react-bootstrap';
import React, {  useEffect, useState } from 'react';
import { DropdownButton } from 'react-bootstrap';
import 'react-calendar/dist/Calendar.css';
import Loading from '../../components/Loading/Loading';
// import AppContext from '../../context/AppContext';
import { getAllTeams } from '../../services/teams.services';
import './CreateWorkshop.css';
import { getAllWorkshops, uploadWorkshop } from '../../services/workshop.services';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router';


const CreateWorkshop = () => {
  const [teams, setTeams] = useState([]);
  const [workshops, setWorkshops] = useState([]);
  const [taskCase, setTaskCase] = useState({description: '', maxPoints: ''});
  // const {user} = useContext(AppContext);
  const [workshop, setWorkshop] = useState({
    title: '',
    teams: [],
    cases : [],
  });
  const navigate = useNavigate();

  useEffect(()=>{
    getAllWorkshops()
      .then(setWorkshops);
    getAllTeams()
      .then(setTeams)
      .catch(console.error());
  }, []);

  const addTeam = (e)=>{
    e.stopPropagation();
    const team= e.target.value;
    setWorkshop({...workshop, teams:[...workshop.teams, team]});
  };


  const removeTeam=(e)=>{
    e.stopPropagation();
    const filtered = workshop.teams.filter((t)=> t!==e.target.value);
    setWorkshop({...workshop, teams:[...filtered]});

  };

  const addCase = (e) => {
    if (e.key!=='Enter') return;
    e.preventDefault();
    const description = taskCase.description.trim();
    const maxPoints = taskCase.maxPoints.trim();
    if (!description || !maxPoints) return;
    const value = `${description} : ${maxPoints}`;

    if (workshop.cases.includes(value)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Assignment criteria already exists!',
      });
      setTaskCase({description: '', maxPoints: ''});
      return;
    }
    setWorkshop({...workshop, cases:[...workshop.cases, value]});
    setTaskCase({description: '', maxPoints: ''});
  };

  const removeCase = (taskCase) => {
    const filtered = workshop.cases.filter((c)=> c!==taskCase);
    setWorkshop({...workshop, cases:[...filtered]});
  };

  const uploadFile = (e) => {
    e.preventDefault();
    const file = e.target[0]?.files?.[0];
    if (!workshop.title)  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please add title!',
      });
      return;
    }

    if (workshop.title.length< 5 || workshop.title.length> 80 )  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please add valid title between 5 and 80 symbols!',
      });
      return;
    }

    if (workshops.find(w=> w.name ===workshop.title))  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'A workshop with this title already exists!',
      });
      return;
    }

    if (!file){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Task File missing!',
      });
      return;
    }
    // setWorkshop({...workshop, file:file});
    uploadWorkshop(workshop.title, [...workshop.teams], [...workshop.cases], file).then(() => {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Workshop created successfully!',
        showConfirmButton: false,
        timer: 1500
      });
      navigate('/dashboard');
    })
      .catch(console.error);
  };

  return (
    <div className='col py-3'>
      <div className='container create-workshop'>
        <h1 className='text-center create-workshop-title'>Create a Workshop</h1>
        <p></p>
        <div className='container-fluid create-workshop-box'>
          <div className='add-workshop-title'>
            <h3>Add Workshop Name</h3>
            <textarea rows="1" className="form-control workshop-title-form" name="description" value={workshop.title} onChange={(e) => setWorkshop({...workshop, title:e.target.value})} ></textarea>
          </div>
          <div className="form-group mb-5">
            <h2>Assessment criteria</h2>
            <div>
              {workshop.cases.map((el, index)=>(
                <div key={index} style={{width: '80%', marginBottom: '1%'}}>
                  <div className='tag-item' style={{backgroundColor: '#03387dc1', color:'whitesmoke', borderRadius:'20px', padding: '.5em .75em'}} key={index}>
                    <span className="text" style= {{display: 'inline'}}>{el}</span>
                    <span className="close" onClick={() => removeCase(el)} style={{float: 'right', marginRight: '1%'}}> &times;</span>
                  </div>
                </div>
              ))}
            </div>
            <div className='row'>
              <div className='col'>
                <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Description</label>
                <input onKeyDown={addCase} type="text" className='tags-input' placeholder='description' style={{marginTop : '2%'}} onChange={(e) => setTaskCase({...taskCase, description: e.target.value})} value={taskCase.description}/>
              </div>
              <div className='col' style={{width: '30%'}}>
                <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Max Points</label>
                <input onKeyDown={addCase} type="text" className='tags-input' placeholder='points' style={{marginTop : '2%'}} onChange={(e) => setTaskCase({...taskCase, maxPoints: e.target.value})} value={taskCase.maxPoints}/>
              </div>
            </div>
          </div>
          <div className='row add-team-section'>
            <div className='col-sm'>
              <h3>Assign to teams</h3>
              <div className='container-fluid team-list' style={{position: 'relative', display: 'block'}}>
                {!teams? <Loading/>: <Dropdown className='team-dropdown'>
                  <DropdownButton className = 'team-dropdown-button' title="Add teams" onChange={(e)=>e.stopPropagation()}>
                    {teams.map(team=> <Dropdown.Item className = 'team-dropdown-item'key={team.id}>
                      <div className='col'><span>{team.name}
                        <button className= 'btn-primary team-add-btn' value={team.id}
                          onClick={workshop.teams.includes(team.id) ? (e)=>removeTeam(e):(e)=> addTeam(e)}>{workshop.teams.includes(team.id)? 'Remove' : 'Add'}
                        </button>
                      </span>
                      </div>
                    </Dropdown.Item>)}
                  </DropdownButton>
                </Dropdown>}
              </div>
            </div>
            <div className='col-sm'>
              <h3>Attach task file</h3>
              <form style={{height: '100px', width: '85%'}} onSubmit={(e)=> uploadFile(e)}>
                <input type="file" className="form-control outline-secondary" />
                <br></br>
                <button className='btn btn-outline-success' type="submit">Submit</button>
              </form>
            </div>

          </div>
          {/* {!user? <Loading/> : <button type="submit" className='btn btn-outline-primary create-btn' data-bs-dismiss="modal" onClick={()=> create(workshop)}>Create workshop</button>} */}
        </div>
      </div>
    </div>
  );
};

export default CreateWorkshop;
