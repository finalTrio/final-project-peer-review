import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router';
import { loginUser } from '../../services/auth.services';
import { getUserData } from '../../services/user.services';
import './Login.css';
import Swal from 'sweetalert2';
import AppContext from '../../context/AppContext';

const Login = () => {
  const navigate = useNavigate();
  const [form, setForm] = useState({email: '', password: ''});
  const { setContext } = useContext(AppContext);

  const submit = (e) => {
    e.preventDefault();

    loginUser(form.email, form.password)
      .then(u => {
        return getUserData(u.user.uid)
          .then(snapshot => {
            if (snapshot.exists()) {
              const data = snapshot.val()[Object.keys(snapshot.val())[0]];
              setContext({
                user: data.username,
                userData: data,
              });
              navigate('/dashboard');
            }
          });
      })


      .catch(() => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Email or password is incorrect!',
        });
      });
  };
  return (
    <div className='container-fluid lg'>
      <div className='form-image'></div>
      <div className="login-form container-fluid">
        <form className= 'login-form-form' onKeyUp={(e) => e.key==='Enter' && submit(e)}>
          <h1>Login</h1>
          <div className="content">
            <div className="input-field">
              <input type="email" placeholder="Email" autoComplete='none' value={form.email} onChange={(e) => setForm({...form, email: e.target.value})} />
            </div>
            <div className="input-field">
              <input type="password" placeholder="Password" autoComplete="new-password" value={form.password} onChange={(e) => setForm({...form, password: e.target.value})} />
            </div>
          </div>
          <div className="action mb-2">
            <button onClick={(e) => submit(e)}>Sign in</button>
            <button onClick={() => navigate('/register')}>Register</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
