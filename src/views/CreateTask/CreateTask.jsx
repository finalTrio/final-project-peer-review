import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { uploadTask } from '../../services/task.services';
import './CreateTask.css';
// import PropTypes from 'prop-types';

const CreateTask = () => {
  const navigate = useNavigate();
  const [tagIn, setTagIn] = useState('');
  const [testCase, setTestCase] = useState({input: '', output: ''});
  const [task, setTask] = useState({
    title: '',
    level: 'Beginner',
    description: '',
    example: '',
    constraints: '',
    params: '',
    cases: [],
    tags: []
  });

  const addTag=(e)=>{
    if(e.key!=='Enter')return;
    e.preventDefault();
    const value= tagIn.trim();
    if (!value) return;
    if (task.tags.includes(value)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Tag name already exists!',
      });
      setTagIn('');
      return;
    }
    setTask({...task, tags:[...task.tags, value]});
    setTagIn('');
  };

  const removeTag = (tag) => {
    const filtered = task.tags.filter((t)=> t!==tag);
    setTask({...task, tags:[...filtered]});
  };

  const addCase = (e) => {
    if (e.key!=='Enter') return;
    e.preventDefault();
    const input = testCase.input.trim();
    const output = testCase.output.trim();
    if (!input || !output) return;
    const value = `${input} => ${output}`;
    if (task.cases.includes(value)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Test case already exists!',
      });
      setTestCase({input: '', output: ''});
      return;
    }
    setTask({...task, cases:[...task.cases, value]});
    setTestCase({input: '', output: ''});
  };

  const removeCase = (testCase) => {
    const filtered = task.cases.filter((c)=> c!==testCase);
    setTask({...task, cases:[...filtered]});
  };

  const submitTask = (e)=>{
    e.preventDefault();
    if (task.title.length < 3 || task.title.length > 30) {
      return(Swal.fire({
        icon: 'error',
        title: 'Invalid title',
        text: 'Task title should be between 5 and 60 symbols!',
      }));
    }

    if (task.description.length < 25 || task.description.length > 2500000) {
      return(Swal.fire({
        icon: 'error',
        title: 'Invalid description',
        text: 'Description should be at least 25 symbols!',
      }));
    }

    uploadTask(task)
      .then(() => navigate('/tasks'))
      .catch(console.error);
  };

  return (
    <div className="col py-3 create-task-back">
      <div className='container-fluid'>
        <div className='row'>
          <h1 className='text-center modal-title'>Create Task</h1>
        </div>
        <div className='container-sm create-task-box'>
          <form onKeyUp={() => false}>
            <div className='row'>
              <div className="col-md-4 create-task-first-col">

                <div className="form-group">
                  <label className='createTaskLabel'>Task Title*</label>
                  <input type="text" className="form-control"
                    placeholder="New Task"
                    value={task.title}
                    onChange={(e) => setTask({...task, title: e.target.value})}/>
                </div>

                <div className="form-group">
                  <label className='createTaskLabel'>Difficulty Level</label>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                    value={task.level}
                    onChange={(e)=>setTask({...task, level:e.target.value})}>
                    <option value="Beginner">Beginner</option>
                    <option value="Intermediate">Intermediate</option>
                    <option value="Advanced">Advanced</option>
                  </select>
                </div>

                <div className="form-group ">
                  <h5 className='createTaskLabel'>Add tags</h5>
                  <div className='tags-input-container'>
                    {task.tags.map((tag, index)=>(
                      <span className='tag-item' style={{backgroundColor: '#03387dc1', color:'whitesmoke', borderRadius:'20px', padding: '.5em .75em'}} key={index}>
                        <span className="text">{tag}</span>
                        <span className="close" onClick={() => removeTag(tag)}> &times;</span>
                      </span>
                    ))}
                    <input onKeyDown={addTag} onChange={(e) => setTagIn(e.target.value)} value={tagIn} type="text" className='tags-input' placeholder='Add a tag' style={{marginTop : '2%'}}/>
                  </div>
                </div>
              </div>
              <div className="col create-task-second-col">
                <div className="form-group">
                  <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Task Description*</label>
                  <textarea className="form-control" id="exampleFormControlTextarea1" rows="5"
                    value={task.description}
                    onChange={(e)=>setTask({...task, description: e.target.value})}></textarea>
                </div>

                <div className="form-group">
                  <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Constraints</label>
                  <textarea className="form-control" id="exampleFormControlTextarea1" rows="5"
                    value={task.constraints}
                    onChange={(e)=>setTask({...task, constraints: e.target.value})}></textarea>
                </div>

                <div className="form-group">
                  <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Examples</label>
                  <textarea className="form-control" id="exampleFormControlTextarea1" rows="5"
                    value={task.example}
                    onChange={(e)=>setTask({...task, example: e.target.value})}></textarea>
                </div>

                <div className="form-group">
                  <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Params</label>
                  <input type="text" className='tags-input' placeholder='Add Params' style={{marginTop : '2%'}} onChange={(e) => setTask({...task, params: e.target.value})} value={task.params}/>
                </div>

                <div className="form-group">
                  <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">Cases</label>
                  <div>
                    {task.cases.map((el, index)=>(
                      <span className='tag-item' style={{backgroundColor: '#03387dc1', color:'whitesmoke', borderRadius:'20px', padding: '.5em .75em'}} key={index}>
                        <span className="text">{el}</span>
                        <span className="close" onClick={() => removeCase(el)}> &times;</span>
                      </span>
                    ))}
                  </div>
                  <div className='row'>
                    <div className='col'>
                      <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">input</label>
                      <input onKeyDown={addCase} type="text" className='tags-input' placeholder='input' style={{marginTop : '2%'}} onChange={(e) => setTestCase({...testCase, input: e.target.value})} value={testCase.input}/>
                    </div>
                    <div className='col'>
                      <label className='createTaskLabel' htmlFor="exampleFormControlTextarea1">output</label>
                      <input onKeyDown={addCase} type="text" className='tags-input' placeholder='output' style={{marginTop : '2%'}} onChange={(e) => setTestCase({...testCase, output: e.target.value})} value={testCase.output}/>
                    </div>
                  </div>
                </div>

                <button type='submit' onClick={(e)=>submitTask(e)} className='btn btn-primary mt-5 submit-task'>Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
  );
};

// CreateTask.propTypes = {}

export default CreateTask;
