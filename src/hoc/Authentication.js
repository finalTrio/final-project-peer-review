/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { useContext } from 'react';
import AppContext from '../context/AppContext';
import { Navigate, useLocation } from 'react-router-dom';
import React from 'react';
import Loading from '../components/Loading/Loading';

export default function Authenticated ({ children, loading, locations }) {
  const { user } = useContext(AppContext);
  const location = useLocation();

  if (loading) {
    return <Loading></Loading>;
  }
  if (locations) {
    if (!user) {
      return <Navigate to={`${locations}`} state={{ from: location }} />;
    }
  }
  return children;
}
