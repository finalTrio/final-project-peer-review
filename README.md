# Application Name: Hogwarts academy

## Link: <a href="https://hogwarts-academy-22.web.app/">Hogwarts academy</a>


## Tasks:

| Unchecked | Checked |
| --------- | ------- |
|  &#9744;  | &#9745; |


### Alex: 

&#9745; auth

&#9745; login service

&#9745; register service

&#9745; scroll to top on different page

&#9745; page component

&#9745; services

&#9745; upload solution (.zip)

&#9745; download solution (.zip)

&#9745; implement console

&#9745; task test cases

&#9745; judge test cases can only be numbers and strings, it cant handle referent types or bool.

&#9745; judge can't accept nested arrays / objects

&#9745; judge can't accept empty string in array / object

&#9744; task info / result


### Vitalia:

&#9745; login view

&#9745; register

&#9745; home view 

&#9745; login/register 

&#9745; login/submit on enter

&#9745; CSS on modal pop-up

&#9745; FAQ Implementation

&#9745; FAQ Design

&#9745; sidebar collapse

&#9745; upload task

&#9744; all tasks

&#9745; forum (comments / tags / post ID)

&#9745; create team (members / teacher)


### Nikol:

&#9745; header and footer- public

&#9745; scroll to top button

&#9745; header background changes on scroll

&#9745; home viewfinal, login and register view

&#9745; Modal pop-up + final

&#9745; Carousel 

&#9745; About us- profile cards

&#9745; FAQ Design

&#9745; Make header responsive- about, programs and etc.

&#9745; HomeFooter uses Footer css options- fixed

&#9745; Tasks view- cards

&#9745; Dashboard- students

&#9745; Forum view

&#9745; Creating posts in forum 

&#9745; icons

&#9745; forum 

&#9745; forum (design)

&#9745; Edit profile form


### Tasks:

&#9744; task completion data
&#9744; exam upload (code from forum)
&#9744; teams
&#9744; admin statistics
&#9744; edit profile (functions)
&#9744; 


### Non priority Tasks:

&#9744; notifications

&#9744; task comments


### Bugs:

&#9744; console style breaks when mode is selected (font is set to the site default)


### Non priority issues:

&#9744; console autocomplete doesn't work

